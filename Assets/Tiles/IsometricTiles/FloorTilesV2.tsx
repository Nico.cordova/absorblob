<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.5.0" name="FloorTilesV2" tilewidth="32" tileheight="32" tilecount="90" columns="15">
 <image source="IsometricFloorTileset.png" width="480" height="192"/>
 <tile id="0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.125" y="8.5">
    <polygon points="-0.053552,1.66011 15.9464,-5.67923 31.4464,1.58853 22.0571,7.64602 15.3578,11.8971 3.21312,3.9329"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.25" y="8.375">
    <polygon points="0.160656,1.98142 15.8571,-5.42923 32.0893,1.82104 24.7348,5.95026 15.3754,10.6654 6.80124,5.91433"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.125" y="9.125">
    <polygon points="0.053552,1.17814 16.0713,-6.2862 31.8392,1.28511 24.3421,4.6467 15.6254,8.30885 6.97966,4.66446"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8.625">
    <polygon points="0.107104,1.60656 15.875,-5.8041 31.3037,1.66011 24.6276,4.89684 16.1074,8.96951 7.40807,4.9325"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="23">
  <objectgroup draworder="index" id="2">
   <object id="2" x="10.4426" y="29.1158">
    <polygon points="-2.40984,-0.293135 -4.81968,-3.60137 -4.65902,-11.851 1.12459,-14.6149 11.0853,-14.6567 15.0481,-11.5579 16.4405,-7.78901 16.7618,-5.06705 16.3334,-2.63821 13.4416,-0.251259"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="30">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.25" y="9.25">
    <polygon points="-0.107104,0.963936 15.8213,-6.28634 31.5,0.803144 23.4851,4.12908 15.7501,7.55913 6.87242,3.93277"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="31">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8.25">
    <polygon points="-0.053552,1.82077 15.8036,-5.42923 31.9999,2.40998 22.9854,6.69985 15.2861,9.64793 6.01572,4.52211"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="45">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.625" y="8.875">
    <polygon points="-0.374864,1.39235 15.3393,-5.67923 30.9821,1.41011 21.9141,5.73578 14.9821,9.09437 6.80083,5.03946"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="46">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.75" y="8.625">
    <polygon points="-0.53552,1.3388 15.232,-5.59002 30.9286,1.48156 23.6098,5.16446 14.8217,8.98727 6.51531,5.05736"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
