<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.5.0" name="FloorTiles" tilewidth="32" tileheight="32" tilecount="2" columns="1">
 <image source="IsometricFloorTile.png" width="32" height="64"/>
 <tile id="1">
  <objectgroup draworder="index" id="3">
   <object id="4" x="6.32494" y="20.7604">
    <polygon points="0,0 1.95363,7.00273 14.9312,7.00273 18.2803,-0.636611 15.4298,-7.89431 2.61781,-8.36112"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
