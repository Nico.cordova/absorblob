<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.5.0" name="FloorTiles2dV2" tilewidth="32" tileheight="32" tilecount="435" columns="15">
 <image source="../SimpleTiles/IslandTilesetV2.png" width="480" height="928"/>
 <tile id="3">
  <objectgroup draworder="index" id="3">
   <object id="2" x="31.875" y="2.75">
    <polygon points="0,0 -31.75,0.25 -31.875,12.5 0,12.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.36364" y="0.272727">
    <polygon points="0,0 0.363636,31.5455 22.4545,31.3636 22.5455,-0.454545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="19">
  <objectgroup draworder="index" id="2">
   <object id="1" x="14.3636" y="0.272727">
    <polygon points="0,0 -0.545455,31.3636 17.5455,31.5455 17.7273,-0.0909091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="20">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.5455" y="0.0909091">
    <polygon points="0,0 0,31.4545 8.45455,31.5455 8.36364,0.0909091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="24">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="32">
    <polygon points="0,0 8.63636,-0.181818 8.09091,-31.9091 0.454545,-31.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="26">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="31.3636">
    <polygon points="0,0 32,-31.4545 31.9091,0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="27">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="0.181818">
    <polygon points="0,0 31.6364,-0.0909091 31.7273,31.7273 -0.0909091,31.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="28">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.272727" y="5.45455">
    <polygon points="0,0 31.1818,16.1818 31.6364,26.3636 -0.181818,26.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="41">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0909091" y="0">
    <polygon points="0,0 0.363636,31.7273 32.3636,31.7273 32.0909,-0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="43">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.0909" y="0.181818">
    <polygon points="0,0 -32.2727,-0.272727 -32.0909,31.7273 -0.181818,31.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="56">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2.36364" y="-0.545455">
    <polygon points="0,0 0.272727,18.0909 17.9091,29.4545 30.4545,30 29.5455,0.272727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="57">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.6364" y="27.8182">
    <polygon points="0,0 -31.5455,0.545455 -31.6364,-27.9091 0.272727,-27.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="58">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="28.0909">
    <polygon points="0,0 17.8182,-3.45455 30.0909,-11.2727 29.4545,-28.1818 0.0909091,-28.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="61">
  <objectgroup draworder="index" id="2">
   <object id="1" x="27.0524" y="31.5831">
    <polygon points="0,0 4.72761,-6.17216 4.72761,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="62">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131322" y="23.5067">
    <polygon points="0.25,-7.75 31.5831,-7.48736 31.6487,5.44988 0.196984,4.3993"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="63">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.393967" y="24.8199">
    <polygon points="0,0 8.73294,6.56612 0.262645,7.09141"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="66">
  <objectgroup draworder="index" id="2">
   <object id="1" x="26.1332" y="30.8608">
    <polygon points="0,0 5.51554,-5.5812 5.71253,1.11624 0,0.984918"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="67">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131322" y="24.8199">
    <polygon points="0,-7.25 31.905,-6.91294 31.7144,6.56612 0,6.50046"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="76">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0656612" y="31.3204">
    <polygon points="0,0 28.9566,-31.78 19.8954,-31.1891 -0.262645,-8.92993"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="78">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4.3993" y="0.262645">
    <polygon points="0,0 27.5777,27.6434 27.4464,20.4863 8.4703,-0.328306"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="81">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.196984" y="31.0578">
    <polygon points="0,0 29.8102,-31.0578 21.4056,-30.9921 -0.131322,-9.19257"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="83">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.71253" y="0">
    <polygon points="0,0 26.1332,26.3958 26.1332,23.638 1.70719,-0.262645"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="90">
  <objectgroup draworder="index" id="2">
   <object id="1" x="24.0977" y="0">
    <polygon points="0,0 0.0656612,32.1083 7.81369,32.1083 7.94501,-0.262645"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="94">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.590951" y="0">
    <polygon points="0,0 0.787935,31.9114 8.20765,31.6487 7.15707,0.0656612"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="95">
  <objectgroup draworder="index" id="2">
   <object id="1" x="25.2139" y="-0.328306">
    <polygon points="0,0 -0.262645,32.0427 6.69745,32.174 6.76311,0.328306"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="99">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131322" y="0">
    <polygon points="0,0 8.33898,0 8.20765,31.8457 -0.0656612,31.9114"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="106">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.262645" y="2.16682">
    <polygon points="0,0 26.1988,29.4819 0,29.6789"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="108">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.51554" y="31.9114">
    <polygon points="0,0 26.3958,-27.9717 26.3302,-0.0656612"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="111">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131322" y="2.29814">
    <polygon points="0,0 26.0018,29.3506 0,29.6132"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="113">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.91485" y="31.78">
    <polygon points="0,0 21.8652,-25.1483 21.9309,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="121">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0656612" y="0.393967">
    <polygon points="0,0 31.7144,31.4517 31.7144,5.18724 23.4411,-0.52529"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="122">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.131322" y="6.89443">
    <polygon points="0,0 32.1083,-0.393967 31.977,24.8199 0.328306,24.8856"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="123">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0656612" y="5.77819">
    <polygon points="0,0 9.45522,-5.71253 31.977,-5.64687 0.328306,26.0675"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="126">
  <objectgroup draworder="index" id="5">
   <object id="4" x="0" y="0.196984">
    <polygon points="0,0 15.693,-0.131322 31.78,13.3949 31.8457,31.6487 0.196984,31.5831"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="127">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="10.3745">
    <polygon points="0,0 31.8457,-0.196984 31.9114,21.4056 0.131322,21.4712"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="128">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0656612" y="13.1979">
    <polygon points="0,0 15.4304,-12.8039 32.0427,-13.1322 31.977,18.6478 0.196984,18.7135"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="141">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131322" y="-0.196984">
    <polygon points="0,0 31.7144,32.0427 31.7144,0.262645"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="142">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131322" y="31.9114">
    <polygon points="0,0 31.6487,-0.131322 31.6487,-31.9114 0,-31.78"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="143">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.196984" y="32.174">
    <polygon points="0,0 31.9114,-32.174 -0.131322,-32.0427"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="312">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131322" y="12.5622">
    <polygon points="0,0 31.5957,-0.1616 31.7144,15.9348 -0.0656612,14.9592"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="317">
  <objectgroup draworder="index" id="2">
   <object id="1" x="18" y="31.9375">
    <polygon points="0,0 3.125,-10.625 13.75,-13 13.8125,-22.5 -9.75,-20.0625 -11.75,-0.625"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="318">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.3125" y="17.5">
    <polygon points="0,0 32.3125,-0.875 32.1875,-17.625 0.25,-17.4375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="319">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.3125" y="16.1875">
    <polygon points="0,0 13.4375,0.875 14.875,15.6875 27.625,15.9375 24.5625,-7.125 -0.0625,-7.625"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="321">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.1875" y="30">
    <polygon points="0,0 32.0625,-15.5 31.6875,-30.0625 -0.1875,-16.5"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="322">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.125" y="15.9375">
    <polygon points="0,0 31.625,0.1875 31.75,-15.75 -0.0625,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="323">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.125" y="16.1875">
    <polygon points="0,0 31.6875,15.6875 32,-13.0625 0.125,-13.4375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="324">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0625" y="31.875">
    <polygon points="0,0 30.5625,-0.1875 0.0625,-31.9375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="325">
  <objectgroup draworder="index" id="2">
   <object id="1" x="27.709" y="31.5831">
    <polygon points="0,0 4.20232,-5.90951 3.93967,0.0656612"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="326">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.459629" y="31.3204">
    <polygon points="0,0 29.7445,-31.3204 8.1706,-31.3797 -0.27529,-24.3113"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="328">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.5812" y="0">
    <polygon points="0,0 26.0018,27.709 26.3238,13.5709 17.0851,0.237355"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="332">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0909091" y="0.181818">
    <polygon points="0,0 15.9148,0 16.0739,31.8182 0,31.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="334">
  <objectgroup draworder="index" id="2">
   <object id="1" x="15.4545" y="0.0909091">
    <polygon points="0,0 -1.18182,31.9091 16.5455,31.8182 16.3636,-0.0909091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="335">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0.181818">
    <polygon points="0,0 31.7273,0.0909091 31.8182,31.5455 0.181818,31.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="339">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.272727" y="0.181818">
    <polygon points="0,0 31.5455,-0.363636 31.5455,31.5455 -0.181818,31.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="340">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.0578" y="31.9114">
    <polygon points="0,0 -17.6697,0.315661 -18.7203,-31.7927 -0.656612,-31.977"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="344">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.787935" y="-0.131322">
    <polygon points="0,0 0.131322,31.6487 16.6809,32.477 16.6152,0.5"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="347">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16.3125" y="0.0625">
    <polygon points="0,0 4.875,8.375 15.5,11.375 15.75,25.9375 -7.0625,25.25 -8,-0.3125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="348">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.1875" y="15.875">
    <polygon points="0,0 32,0.375 32.0625,16.1875 -0.0625,16.1875"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="349">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0625" y="13.125">
    <polygon points="0,0 13.8125,-3.0625 14.625,-13.1875 30.25,-12.8125 26.875,14.3125 0.1875,14.9375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="351">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.1875" y="6">
    <polygon points="0,0 31.5625,10.375 31.875,25.9375 -0.1875,25.875"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="352">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="15.5625">
    <polygon points="0,0 31.625,0.125 31.9375,0.25 32,16.25 0.125,16.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="353">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16.25">
    <polygon points="0,0 32.0625,-16.125 32,15.625 -0.0625,15.625"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="356">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.196984" y="0.393967">
    <polygon points="0,0 28.3657,31.4517 13.2636,31.6487 -0.131322,31.5174"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="358">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.71253" y="31.5174">
    <polygon points="0,0 26.1332,-27.8404 26.0675,0.262645 -0.393967,0.459629"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="371">
  <objectgroup draworder="index" id="2">
   <object id="1" x="25.4109" y="-0.0656612">
    <polygon points="0,0 6.56612,5.77819 6.36914,32.0427 -4.46496,31.8457 -25.7392,8.01067 -25.4109,-0.0656612"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="372">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.131322" y="2.42947">
    <polygon points="0,0 32.1083,0.131322 32.0427,29.4819 0.393967,29.3506"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="373">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="5.44988">
    <polygon points="0,0 5.90951,-5.51554 31.78,-5.5812 32.0427,11.622 13.5919,26.4615 0.196984,26.4615"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="380">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22.4561" y="0">
    <polygon points="0,0 -1.1819,31.8457 -9.12691,31.977 -7.48538,-0.262645"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="381">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.58654" y="0">
    <polygon points="0,0 -0.0656612,31.8457 6.17216,31.78 6.56612,-0.196984"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="392">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.1083" y="17.4002">
    <polygon points="0,0 -17.9912,0.0656612 -17.5972,14.5768 -0.262645,14.4455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="393">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0656612" y="16.7436">
    <polygon points="0.1875,-0.125 31.9114,-0.459629 32.0522,15.2032 0.115516,15.284"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="394">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.459629" y="16.481">
    <polygon points="0,0 18.5821,1.31322 17.5972,15.1021 10.5715,15.2991 10.3745,8.73294 -0.0656612,8.33898"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="395">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.44455" y="0">
    <polygon points="0,0 0.52529,31.9114 7.35406,31.9114 7.09141,-0.262645"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="396">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.7037" y="0.196984">
    <polygon points="0,0 -0.393967,31.5174 5.38422,31.7144 6.10649,-0.459629"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="407">
  <objectgroup draworder="index" id="2">
   <object id="1" x="15.3647" y="-0.0656612">
    <polygon points="0,0 -0.853596,31.78 16.678,31.8457 16.6123,0.196984"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="409">
  <objectgroup draworder="index" id="2">
   <object id="1" x="17.2689" y="0.262645">
    <polygon points="0,0 -0.853596,31.8457 -7.74803,31.5174 -6.4348,-0.590951"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="410">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.984918" y="-0.0656612">
    <polygon points="0,0 0.328306,31.977 7.35406,32.1083 6.82877,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="411">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22.5875" y="0">
    <polygon points="0,0 0.196984,31.8457 7.41972,31.9114 7.09141,-0.0656612"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="422">
  <objectgroup draworder="index" id="2">
   <object id="1" x="13.8545" y="0.131322">
    <polygon points="0,0 0.196984,14.5111 17.9912,14.7738 17.9912,-0.196984"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="423">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.8457" y="15.2991">
    <polygon points="0,0 -31.9114,0.131322 -31.8457,-15.3647 0.0656612,-15.2991"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="424">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.196984" y="15.2991">
    <polygon points="0,0 17.4659,-0.262645 17.0063,-15.3647 -0.131322,-15.1677"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
