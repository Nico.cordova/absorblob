using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static DigestiveSystem;
using System.Linq;

public class FoodGod : GenericDestroyableSingletonClass<FoodGod>
{
    private List<Consumable> consumables = new List<Consumable>();
    private void Start()
    {
        consumables = FindObjectsOfType<Consumable>().ToList();
    }

    public Vector3 HintFood(List<FoodType> eatableFood)
    {
        if (consumables.Count == 0)
        {
            consumables = FindObjectsOfType<Consumable>().ToList();
        }
        Vector3 deviationVector = new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0);
        return consumables.Find(consumable => eatableFood.Contains(consumable.foodType) && consumable.canBeEaten).transform.position + deviationVector;
    }
}
