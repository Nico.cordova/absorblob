using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Hitable : MonoBehaviour
{
    [Header("Configuration")]
    public float knockbackDistance = 1f;
    [Range(0, 1)]
    public float instaKillThreshold;
    public DoTweenController doTweenController;

    private Rigidbody2D _rigidBody;

    private Vector3 _lastDamageOrigin = Vector3.zero;
    // Start is called before the first frame update
    public virtual void Start()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody2D>();
        doTweenController = gameObject.GetComponent<DoTweenController>();
    }

    public abstract void GetHit(float damage, Vector3 sourceOfDamage);
    public abstract void Die();
    public void KnockBack(Vector2 sourceOfDamage)
    {
        if (sourceOfDamage == Vector2.zero)
        {
            Vector2 difference = _lastDamageOrigin - transform.position;
            doTweenController.MoveToTarget(-difference.normalized * knockbackDistance + (Vector2)transform.position, true);
        }
        else
        {
            Vector2 difference = sourceOfDamage - (Vector2)transform.position;
            doTweenController.MoveToTarget(-difference.normalized * knockbackDistance + (Vector2)transform.position, true);
            _lastDamageOrigin = sourceOfDamage;
        }
    }
}
