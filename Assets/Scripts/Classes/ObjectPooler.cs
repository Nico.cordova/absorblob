using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ObjectPooler : GenericDestroyableSingletonClass<ObjectPooler>
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;
    // Start is called before the first frame update
    void Start()
    {
        Setup();
    }
    public void Setup()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            GameObject[] objects = GameObject.FindGameObjectsWithTag(pool.tag);
            foreach (GameObject poolObject in objects)
            {
                objectPool.Enqueue(poolObject);
                poolObject.SetActive(false);
            }
            for (int i = objectPool.Count; i < pool.size; i++)
            {
                GameObject poolObject = GameObject.Instantiate(pool.prefab, transform.position + new Vector3(0, -20, 0), Quaternion.identity);
                poolObject.SetActive(false);
                objectPool.Enqueue(poolObject);
            }
            poolDictionary.Add(pool.tag, objectPool);
        }
    }
    public GameObject SpawnFromPool (string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning($"POOL WITH TAG {tag} DOESNT EXISTS");
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        poolDictionary[tag].Enqueue(objectToSpawn);
        objectToSpawn.SetActive(true);
        return objectToSpawn;
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Setup();
    }
}
