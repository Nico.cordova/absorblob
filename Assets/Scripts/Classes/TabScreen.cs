using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TabScreen : MonoBehaviour
{
    [SerializeField] private Animator animator;

    private void Start()
    {
        if (!animator)
        {
            animator = gameObject.GetComponent<Animator>();
        }
    }
    public void SlideIn() 
    {
        animator.SetTrigger("SlideIn");
    }
    public void SlideOut() 
    {
        animator.SetTrigger("SlideOut");
    }
    public bool IsActive()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("SlideIn"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public abstract void Show();
    public abstract void Hide();
    public abstract void OnShowAnimationEnd();
    public abstract void OnShowAnimationStart();
    public abstract void OnHideAnimationEnd();
    public abstract void OnHideAnimationStart();
}
