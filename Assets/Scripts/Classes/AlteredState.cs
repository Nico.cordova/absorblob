﻿using System;
using UnityEngine;

public enum AlteredStateType { POISON, FIRE, FREEZE }
[Serializable, CreateAssetMenu(menuName = "Debuff/AlteredState", fileName = "New Altered State Modifier")]
public class AlteredState: PowerUpEffect
{
    public float numberOfTicks;
    public int damage;
    public float ticksPerSecond;
    public AlteredStateType type = AlteredStateType.POISON;
    public override void Apply(GameObject gameObject)
    {
    }
}