﻿using System;
using UnityEngine;

public enum AttackModifierType { FIRE, WATER, POISON, PARALYSIS, DOUBLE, MELEE, RANGED }
[Serializable, CreateAssetMenu(menuName = "PowerUps/AttackModifier", fileName ="New Attack Modifier")]
public class AttackModifier: PowerUpEffect
{
    public bool enablesAttack = false;
    public float duration = 3f;
    public AttackModifierType type = AttackModifierType.MELEE;
    public override void Apply(GameObject gameObject)
    {
        if (gameObject.CompareTag("Player"))
        {
            BlobStats blobStats = gameObject.GetComponent<BlobStats>();
            blobStats.AddAttackModifier(this);
        }
    }
}