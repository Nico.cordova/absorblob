﻿using System;
using UnityEngine;

public enum StatsModifierType { STOMACH, HEART, DAMAGE }
[Serializable, CreateAssetMenu(menuName = "PowerUps/StatsModifier", fileName = "New Stats Modifier")]
public class StatsModifier: PowerUpEffect
{
    public float amount = 1f;
    public float duration = 3f;
    public StatsModifierType type = StatsModifierType.HEART;

    public override void Apply(GameObject gameObject)
    {
        
    }
}