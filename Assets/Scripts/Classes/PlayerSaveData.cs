using System.Collections;
using UnityEngine;

[System.Serializable]
public class PlayerSaveData 
{
    public int completedLevels = 0;
    public float secondsAlive = 0;
    public float[] position;
    public float heartsLeft;
    public float caloriesLeft;
    public int deaths;

    public PlayerSaveData()
    {
        completedLevels = GameManager.Instance.GetCompletedLevels().Count;
        secondsAlive = GameManager.Instance.timeAlive;
        position = new float[3];
        position[0] = GameManager.Instance.mainPlayer.transform.position.x;
        position[1] = GameManager.Instance.mainPlayer.transform.position.y;
        position[2] = GameManager.Instance.mainPlayer.transform.position.z;
        heartsLeft = GameManager.Instance.mainPlayer.stats.HeartsLeft();
        caloriesLeft = GameManager.Instance.mainPlayer.CaloriesLeft();
        deaths = GameManager.Instance.deaths;
    }

    public void LoadData(bool deathsOnly=false)
    {
        GameManager.Instance.deaths = deaths;
        if (!deathsOnly)
        {
            PlayerController mainPlayer = GameManager.Instance.mainPlayer;
            completedLevels = GameManager.Instance.GetCompletedLevels().Count;
            while (GameManager.Instance.GetCompletedLevels().Count <= completedLevels)
            {
                GameManager.Instance.CompleteLevel();
                GameManager.Instance.SetNextLevel();
            }

            mainPlayer.transform.position = new Vector3(position[0], position[1], position[2]);
            if (heartsLeft != mainPlayer.stats.HeartsLeft())
            {
                float diff = heartsLeft - mainPlayer.stats.HeartsLeft();
                if (diff < 0)
                {
                    mainPlayer.stats.ReduceHearts(-diff);
                }
                else
                {
                    mainPlayer.stats.AddHearts(diff);
                }
            }
            caloriesLeft = GameManager.Instance.mainPlayer.CaloriesLeft();
            if (caloriesLeft != mainPlayer.CaloriesLeft())
            {
                float diff = caloriesLeft - mainPlayer.CaloriesLeft();
                if (diff < 0)
                {
                    mainPlayer.ReduceCalories(-diff);
                } else
                {
                    mainPlayer.AddCalories(diff);
                }
            }
        }
    }
}
