using UnityEngine;
using System;

public class Building : Hitable
{
    public event Action<Building> OnDie;
    [SerializeField] private float defaultHealth = 200;
    [SerializeField] private Animator animator;
    private bool alive = true;
    private float actualHealth;
    public override void Start()
    {
        base.Start();
        actualHealth = defaultHealth;
        if (animator == null)
        {
            animator = gameObject.GetComponent<Animator>();
        }
    }
    public override void Die()
    {
        alive = false;
        OnDie?.Invoke(this);
        animator.SetTrigger("Die");
    }
    public bool IsAlive() => alive;
    public override void GetHit(float damage, Vector3 sourceOfDamage)
    {
        if (alive)
        {
            actualHealth -= damage;
            GameObject damagePopUpGO = ObjectPooler.Instance.SpawnFromPool("DamagePopUp", transform.position, transform.rotation);
            DamagePopUp damagePopUp = damagePopUpGO.GetComponent<DamagePopUp>();
            damagePopUp.Initialize(damage);
            if (actualHealth <= 0)
            {
                Die();
            }
        }
    }
}
