using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAIPath : AIPath
{
    private Enemy _enemyController;
    private DigestiveSystem _digestiveSystem;
    protected override void Start()
    {
        base.Start();
        _digestiveSystem = gameObject.GetComponent<DigestiveSystem>();
        _enemyController = gameObject.GetComponent<Enemy>();
    }
    protected override void OnDisable()
    {
        base.OnDisable();
    }
    public override void OnTargetReached()
    {
        base.OnTargetReached();
        if (_enemyController.isEating)
        {
            _enemyController.AnimateEating();
            enabled = false;
        }
    }
}
