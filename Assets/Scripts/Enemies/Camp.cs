using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Camp : MonoBehaviour
{
    [SerializeField] private List<Building> buildings = new List<Building>();
    public event Action<Camp> OnCampDie; 
    private bool alive = true;
    private void Start()
    {
        buildings = GetComponentsInChildren<Building>().ToList();
        foreach (Building building in buildings)
        {
            building.OnDie += HandleBuildingDying; 
        }
    }
    public bool IsAlive() => alive;
    private void HandleBuildingDying(Building dying_building) 
    {
        List<Building> aliveBuildings = buildings.FindAll(building => building.IsAlive());
        if (aliveBuildings.Count == 0)
        {
            alive = false;
            OnCampDie?.Invoke(this);
        }
    }
    private void OnDestroy()
    {
        foreach (Building building in buildings)
        {
            building.OnDie -= HandleBuildingDying;
        }
    }
}
