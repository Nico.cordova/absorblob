using UnityEngine;
using System.Linq;
using Pathfinding;
using System;
using Random = UnityEngine.Random;
using System.Collections;
using System.Collections.Generic;

public class Enemy : Hitable
{
    public event Action<Enemy> OnEnemyDie;

    [Header("Configuration")]
    public float defaultGuidanceCooldown = 4f;
    public float defaultOutOfCombatDelay = 10f;
    public int defaultAttackDamage = 1;
    public float defaultHealth;
    [Range(0, 1)]
    public float dyingThreshold;
    public bool isUnderAttack;
    public bool isAggressive;
    public bool isEating;
    public AIPath pathfinder;

    [SerializeField]
    private Animator _animator;
    private bool isAttacking;
    private float currentHealth;
    private Rigidbody2D _rigidbody;
    private DigestiveSystem digestiveSystem;
    private float guidanceCooldown;
    private float outOfCombatDelay;
    private SpriteRenderer spriteRenderer;
    private Consumable nearestConsumable;
    private GameObject target;
    private MaterialTintColor materialTintColor;
    private Consumable ownConsumableStats;
    public void Setup() 
    {
        digestiveSystem = gameObject.GetComponent<DigestiveSystem>();
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();
        pathfinder = gameObject.GetComponent<AIPath>();
        spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
        materialTintColor = gameObject.GetComponentInChildren<MaterialTintColor>();
        ownConsumableStats = gameObject.GetComponent<Consumable>();
        guidanceCooldown = defaultGuidanceCooldown;
        outOfCombatDelay = defaultOutOfCombatDelay;
        currentHealth = defaultHealth;
        ownConsumableStats.canBeEaten = false;

        _rigidbody.isKinematic = false;
    }
    private void Awake()
    {
        Setup();
    }
    public override void Start()
    {
        base.Start();
        Setup();
        ownConsumableStats.OnEaten += HandleGetEaten;
    }
    private void ActivatePathfinder()
    {
        pathfinder.enabled = true;
    }
    public bool IsAttacking()
    {
        return isAttacking;
    }
    public void SetAttacking(bool state)
    {
        isAttacking = state;
    }
    public bool Alive()
    {
        return currentHealth > 0;
    }
    public GameObject GetTarget()
    {
        return target;
    }
    public Consumable GetNearestConsumable()
    {
        if (digestiveSystem.consumablesInRange.Count != 0)
        {
            Consumable nearest = digestiveSystem.consumablesInRange.Aggregate((curNearest, consumable) => (curNearest == null || (consumable.transform.position - transform.position).magnitude < (curNearest.transform.position - transform.position).magnitude ? consumable : curNearest)); ;
            return nearest;
        } else
        {
            return null;
        }
        
    }
    public void LookAtTarget()
    {
        if (currentHealth <= 0)
            return;
        if (!target)
        {
            target = GameManager.Instance.mainPlayer.gameObject;
        }
        Vector3 targetDirection = target.transform.position - transform.position;
        if (targetDirection.x < 0)
        {
            _animator.Play("Idle");
        }
        else if (targetDirection.x >= 0)
        {
            _animator.Play("IdleRight");
        }
    }
    public bool IsHungry()
    {
        return digestiveSystem.IsHungry();
    }
    public bool EatenEnough()
    {
        return digestiveSystem.EatenEnough();
    }
    public bool IsDying()
    {
        return currentHealth / defaultHealth > dyingThreshold;
    }
    private void Update()
    {
        _animator.SetFloat("Speed", pathfinder.velocity.sqrMagnitude);
        _animator.SetFloat("Horizontal", pathfinder.desiredVelocity.x);
        _animator.SetFloat("Vertical", pathfinder.desiredVelocity.y);
        if (guidanceCooldown > 0f)
        {
            guidanceCooldown -= Time.deltaTime;
            guidanceCooldown = Mathf.Clamp(guidanceCooldown, 0, Mathf.Infinity);
        }
        if ( isUnderAttack && outOfCombatDelay > 0f)
        {
            outOfCombatDelay -= Time.deltaTime;
            outOfCombatDelay = Mathf.Clamp(outOfCombatDelay, 0, Mathf.Infinity);
        } else if (isUnderAttack && outOfCombatDelay == 0f)
        {
            outOfCombatDelay = defaultOutOfCombatDelay;
            isUnderAttack = false;
        } else if (!isUnderAttack && outOfCombatDelay < defaultOutOfCombatDelay)
        {
            outOfCombatDelay = defaultOutOfCombatDelay;
        }
    }
    public void SearchForFood()
    {
        if (pathfinder.velocity.magnitude <= 0.01f && !isEating)
        {
            Vector3 latestSeen = digestiveSystem.GetLatestConsumableSeen();
            Vector3 latestPlaceEatenAt = digestiveSystem.GetLatestPlaceIAte();
            Vector3 deviationVector = new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0);
            if (digestiveSystem.ConsumableInRange())
            {
                MoveToNearestConsumable();
            }
            else if (latestSeen.x != Mathf.NegativeInfinity)
            {
                pathfinder.destination = latestSeen + deviationVector;
            }
            else if (latestPlaceEatenAt.x != Mathf.NegativeInfinity)
            {
                pathfinder.destination = latestPlaceEatenAt + deviationVector;
            } else if (guidanceCooldown == 0f)
            {
                Vector3 guidance = AskForGuidance();
                pathfinder.destination = guidance;
            } else
            {
                MoveRandomly(5f);
            }
        }
    }

    public void MoveToNearestConsumable()
    {
        nearestConsumable = GetNearestConsumable();
        if (nearestConsumable != null)
        {
            isEating = true;
            Vector3 distance = nearestConsumable.transform.position - transform.position;
            if (distance.x < 0)
            {
                pathfinder.destination = nearestConsumable.transform.position + new Vector3(-spriteRenderer.size.x/2, 0, 0);
            } else
            {
                pathfinder.destination = nearestConsumable.transform.position + new Vector3(spriteRenderer.size.x / 2, 0, 0);
            }
        }
    }
    public void AnimateEating()
    {
        Consumable nearestConsumable = GetNearestConsumable();
        if (nearestConsumable != null)
        {
            Vector3 distance = nearestConsumable.transform.position - transform.position;
            if (distance.x < 0)
            {
                _animator.SetTrigger("EatLeft");
            }
            else
            {
                _animator.SetTrigger("EatRight");
            }
        }
        else {
            pathfinder.enabled = true;
            SearchForFood();
        };
    }
    public void Swallow()
    {
        isEating = false;
        pathfinder.enabled = true;
    }
    override public void GetHit(float damage, Vector3 sourceOfDamage)
    {
        if (currentHealth <= 0)
        {
            return;
        }
        materialTintColor.SetTintColor(new Color(1, 1, 1, 1f));
        KnockBack((Vector2)sourceOfDamage);
        currentHealth -= damage;
        GameObject damagePopUpGO = ObjectPooler.Instance.SpawnFromPool("DamagePopUp", transform.position, transform.rotation);
        DamagePopUp damagePopUp = damagePopUpGO.GetComponent<DamagePopUp>();
        damagePopUp.Initialize(damage);
        if (currentHealth <= 0)
        {
            Die();
        }
        else
        {
            _animator.SetTrigger("Hit");
        }
        target = FindObjectOfType<PlayerController>().gameObject;
        isUnderAttack = true;
    }
    override public void Die()
    {
        pathfinder.canMove = false;
        _animator.SetTrigger("Die");
        ownConsumableStats.canBeEaten = true;
        _rigidbody.isKinematic = true;
        _rigidbody.velocity = Vector2.zero;
        OnEnemyDie?.Invoke(this);
    }
    public Vector3 AskForGuidance()
    {
        return FoodGod.Instance.HintFood(digestiveSystem.foodICanEat);
    }
    public void MoveRandomly(float radius)
    {
        StartCoroutine(MoveRandomlyTask(radius));
    }
    public IEnumerator MoveRandomlyTask(float radius)
    {
        Vector3 deviationVector = new Vector3(Random.Range(-radius, radius), Random.Range(-radius, radius), 0);
        RaycastHit2D hit = Physics2D.Raycast(deviationVector, transform.position);
        if (hit.collider != null)
        {
            while (hit.collider.gameObject.CompareTag("Bush") || hit.collider.gameObject.CompareTag("Wall") || hit.collider.gameObject.CompareTag("Water"))
            {
                deviationVector = new Vector3(Random.Range(-radius, radius), Random.Range(-radius, radius), 0);
                hit = Physics2D.Raycast(deviationVector, transform.position);
            }
        }
        pathfinder.destination = transform.position + deviationVector;
        yield break;
    }
    public void HandleGetEaten()
    {
        List<SpriteRenderer> sameLevelRenderes = gameObject.GetComponents<SpriteRenderer>().ToList();
        List<SpriteRenderer> childrenLevelRenderes = gameObject.GetComponentsInChildren<SpriteRenderer>().ToList();
        List<SpriteRenderer> renderers = sameLevelRenderes.Concat(childrenLevelRenderes).ToList();
        renderers.ForEach(renderer => renderer.enabled = false);
    }
    private void OnDestroy()
    {
        ownConsumableStats.OnEaten -= HandleGetEaten;
    }
}
