using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float damage;
    public int maxHits = 2;
    [SerializeField]
    Collider2D _collider;
    [SerializeField]
    Rigidbody2D _rigidbody;
    [SerializeField]
    ProjectileType _projectileType;

    int _currentHits;

    public enum ProjectileType { PLAYER, ENEMY };
    public void Awake()
    {
        _currentHits = 0;
    }
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }
    public void InitializeAsPlayer() {
        _projectileType = ProjectileType.PLAYER;
        gameObject.layer = LayerMask.NameToLayer("PlayerProjectile");
    }
    public void InitializeAsEnemy() {
        _projectileType = ProjectileType.ENEMY;
        gameObject.layer = LayerMask.NameToLayer("EnemyProjectile");
    }
    public void Shoot(float shootForce, Vector3 direction)
    {
        _rigidbody.AddForce(direction * shootForce, ForceMode2D.Impulse);
        StartCoroutine(DelayedClean());
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        _currentHits += 1;
        if (_projectileType == ProjectileType.PLAYER && collision.gameObject.CompareTag("Enemy"))
        {
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            enemy.GetHit(damage, transform.position);
        } else if (_projectileType == ProjectileType.PLAYER && collision.gameObject.CompareTag("Building"))
        {
            Building building = collision.gameObject.GetComponent<Building>();
            building.GetHit(damage, transform.position);
        }
        else if (_projectileType == ProjectileType.ENEMY && collision.gameObject.CompareTag("Player"))
        {
            PlayerController blob = collision.gameObject.GetComponent<PlayerController>();
            if (blob.stats.perfectBlocking)         
            {
                var opposite = -_rigidbody.velocity;
                InitializeAsPlayer();
                _rigidbody.AddForce(opposite*150f);
                damage = blob.stats.GetAttackDamage();
                blob.ResetVelocity();
            }
            else if (blob.stats.blocking)
            {
                _currentHits += 1;
            }
            else
            {
                blob.GetHit(damage, transform.position);
                _currentHits += 1;
            }
        }
        
        if (_currentHits == maxHits)
        {
            gameObject.SetActive(false);
        }
    }
    public IEnumerator DelayedClean()
    {
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
    }
}
