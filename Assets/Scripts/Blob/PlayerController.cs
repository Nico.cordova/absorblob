using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;
public class PlayerController : Hitable
{
    [Header("Input and Movement")]
    public InputMaster inputControls;
    public float torqueForce = 2f;
    public float distanceMoveForward = 1f;
    public float moveForce = 1f;
    public float defaultMoveSpeed = 3f;
    public float maxSpeed = 2f;
    public bool eatDetected = false;
    public int attacksDetected = 0;
    public bool attackInputsDisabled = false;
    public GameObject facingIndicator;

    [Header("Components")]
    public BlobStats stats;
    public Collider2D attackCollider;
    [SerializeField]
    private Rigidbody2D rigidBody;
    [SerializeField]
    private Animator _animator;
    [SerializeField] private Shooting shooting;

    private Vector2 movement;
    private DigestiveSystem digestiveSystem;
    private float moveSpeed;
    [SerializeField] private float energyConsumeCountdown = .3f;

    [Header("DEBUGGING")]
    [SerializeField]
    private List<Enemy> enemiesInRange = new List<Enemy>();
    [SerializeField]
    private List<Building> buildingsInRange = new List<Building>();
    [SerializeField]
    private List<GameObject> obstaclesInRange = new List<GameObject>();
    [SerializeField]
    private List<GameObject> environmentAssetsInRange = new List<GameObject>();
    private Vector2 lastFacingDirection = Vector2.up;
    public void Setup()
    {
        if (digestiveSystem == null)
        {
            digestiveSystem = gameObject.GetComponent<DigestiveSystem>();
        }
        StopAllCoroutines();
        stats.Setup();
        digestiveSystem.Setup();
        moveSpeed = defaultMoveSpeed;
    }
    public void ResetVelocity()
    {
        rigidBody.velocity = Vector2.zero;
        moveSpeed = defaultMoveSpeed;
    }
    public void ResetSpeed()
    {
        moveSpeed = defaultMoveSpeed;
    }
    public void SlowDown(float amount)
    {
        moveSpeed = defaultMoveSpeed * (1 - amount);
    }
    private void Awake()
    {
        inputControls = new InputMaster();
        inputControls.Player.Moving.performed += ctx => Move(ctx);
        inputControls.Player.Moving.canceled += ctx => StopMoving(ctx);
        inputControls.Player.Facing.performed += ctx => Face(ctx);
        inputControls.Player.Attacking.performed += ctx => Attack(ctx);
        inputControls.Player.Shooting.performed += ctx => Shoot(ctx);
        inputControls.Player.Blocking.performed += ctx => Block(ctx);
        inputControls.Player.Blocking.canceled += ctx => StopBlocking(ctx);
        inputControls.Player.Eat.performed += ctx => EatNearest(ctx);
        inputControls.Player.Pause.performed += ctx => OnPauseAction(ctx);
    }
    private void FixedUpdate()
    {
        if (movement != Vector2.zero && (!stats.beingHit || !stats.eating))
        {
            if (stats.isHardenned && rigidBody.velocity.magnitude < maxSpeed)
            {
                energyConsumeCountdown -= Time.fixedDeltaTime;
                if (energyConsumeCountdown < 0)
                {
                    digestiveSystem.ConsumeCalories(stats.hardenningEnergyCost);
                    energyConsumeCountdown = 1 + energyConsumeCountdown;
                }
                rigidBody.AddForce(movement * moveForce);
            }
        }
    }
    private void Update()
    {
        if (movement != Vector2.zero && (!stats.beingHit || !stats.eating))
        {
            if (!stats.isHardenned)
            {
                rigidBody.MovePosition(rigidBody.position + movement * moveSpeed * Time.deltaTime);
            }
        }
    }
    public override void Start()
    {
        base.Start();
        rigidBody = gameObject.GetComponent<Rigidbody2D>();
        _animator = gameObject.GetComponent<Animator>();
        digestiveSystem = gameObject.GetComponent<DigestiveSystem>();
        Setup();
    }
    private void OnEnable()
    {
        inputControls.Enable();
    }
    private void OnDisable()
    {
        inputControls.Disable();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            Enemy enemyController = collision.GetComponent<Enemy>();
            enemiesInRange.Add(enemyController);
        }
        else if (collision.CompareTag("Building"))
        {
            Building buildingController = collision.GetComponent<Building>();
            if (!buildingsInRange.Contains(buildingController))
            {
                buildingsInRange.Add(buildingController);
            };
        }
        else if (collision.CompareTag("Wall"))
        {
            obstaclesInRange.Add(collision.gameObject);
        } else if (collision.CompareTag("EnvironmentAsset"))
        {
            environmentAssetsInRange.Add(collision.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            Enemy enemyController = collision.GetComponent<Enemy>();
            if (enemiesInRange.Contains(enemyController))
            {
                enemiesInRange.Remove(enemyController);
            }
        }
        else if (collision.CompareTag("Building"))
        {
            Building buildingController = collision.GetComponent<Building>();
            if (buildingsInRange.Contains(buildingController))
            {
                buildingsInRange.Remove(buildingController);
            };
        }
        else if (collision.CompareTag("Wall"))
        {
            if (obstaclesInRange.Contains(collision.gameObject))
                obstaclesInRange.Remove(collision.gameObject);
        }
        else if (collision.CompareTag("EnvironmentAsset"))
        {
            if (environmentAssetsInRange.Contains(collision.gameObject))
                environmentAssetsInRange.Remove(collision.gameObject);
        }
    }
    public void OnPauseAction(InputAction.CallbackContext context)
    {
        if (PlayerGUI.Instance.pauseMenu.IsActive())
        {
            Time.timeScale = 1f;
            PlayerGUI.Instance.pauseMenu.SlideOut();
        }
        else
        {
            PlayerGUI.Instance.pauseMenu.SlideIn();
        }
    }
    private void Move(InputAction.CallbackContext context)
    {

        Vector2 direction = context.ReadValue<Vector2>();
        movement = direction;
        if (direction.magnitude >= 0.20f) {
            _animator.SetFloat("Vertical", movement.y);
            _animator.SetFloat("Horizontal", movement.x);
            _animator.SetFloat("Speed", movement.sqrMagnitude);
        }
        else
        {
            movement = Vector2.zero;
        }
    }
    private void StopMoving(InputAction.CallbackContext context)
    {
        movement = Vector2.zero;
        _animator.SetFloat("LatestHorizontal", _animator.GetFloat("Horizontal"));
        _animator.SetFloat("Vertical", 0);
        _animator.SetFloat("Horizontal", 0);
        _animator.SetFloat("Speed", 0);
        if (!stats.isHardenned && !stats.beingHit)
            rigidBody.velocity = Vector2.zero;
    }
    private void Attack(InputAction.CallbackContext context)
    {
        if (!attackInputsDisabled && stats.canAttack && attacksDetected < 3)
        {
            attacksDetected += 1;
        }
    }
    private void Shoot(InputAction.CallbackContext context)
    {
        if (stats.canShoot)
        {
            shooting.Shoot(lastFacingDirection);
        }
    }

    private void EatNearest(InputAction.CallbackContext context)
    {
        Consumable nearest = digestiveSystem.GetNearestConsumable();
        if ( stats.canEat && !eatDetected && !stats.eating && digestiveSystem.CanEat(nearest))
        {
            inputControls.Disable();
            eatDetected = true;
            stats.eating = true;
            ResetVelocity();
            Vector3 targetDirection = nearest.transform.position - transform.position;
            if (targetDirection.x < 0)
            {
                _animator.SetTrigger("EatLeft");
            }
            else if (targetDirection.x >= 0)
            {
                _animator.SetTrigger("EatRight");
            }
            doTweenController.MoveToTarget(nearest.transform.position);
        }
    }
    private void Block(InputAction.CallbackContext context)
    {
        attacksDetected = 0; 
        stats.isHardenned = true;
        _animator.SetBool("Harden", stats.isHardenned);
        _animator.Play("Harden");
        return;
    }
    private void StopBlocking(InputAction.CallbackContext context)
    {
        stats.isHardenned = false;
        _animator.SetBool("Harden", stats.isHardenned);
        ResetVelocity();
    }
    private void ResetFacingPosition()
    {
        _animator.SetBool("FF", false);
        _animator.SetBool("FFL", false);
        _animator.SetBool("FFR", false);
        _animator.SetBool("FL", false);
        _animator.SetBool("FR", false);
        _animator.SetBool("FB", false);
        _animator.SetBool("FBL", false);
        _animator.SetBool("FBR", false);
    }
    private void Face(InputAction.CallbackContext context)
    {
        Vector2 direction = context.ReadValue<Vector2>();
        lastFacingDirection = direction;
        float angle = Vector2.SignedAngle(direction, Vector2.up);
        facingIndicator.transform.rotation = Quaternion.Euler(0, 0, -angle);
        if (angle > -22.5f && angle <= 22.5f && !_animator.GetBool("FB"))
        {
            ResetFacingPosition();
            _animator.SetBool("FB", true);
            attackCollider.transform.eulerAngles = new Vector3(0, 0, 270f);
        } else if (angle > -67.5f && angle <= -22.5f && !_animator.GetBool("FBL"))
        {
            ResetFacingPosition();
            _animator.SetBool("FBL", true);
            attackCollider.transform.eulerAngles = new Vector3(0, 0, 315f);
        } else if (angle > -112.5f && angle <= -67.5f && !_animator.GetBool("FL"))
        {
            ResetFacingPosition();
            _animator.SetBool("FL", true);
            attackCollider.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (angle > -157.5f && angle <= -112.5f && !_animator.GetBool("FFL"))
        {
            ResetFacingPosition();
            _animator.SetBool("FFL", true);
            attackCollider.transform.eulerAngles = new Vector3(0, 0, 45f);
        }
        else if (angle > -180f && angle <= -157.5f && !_animator.GetBool("FF") || angle > 157f && angle <= 180f && !_animator.GetBool("FF"))
        {
            ResetFacingPosition();
            _animator.SetBool("FF", true);
            attackCollider.transform.eulerAngles = new Vector3(0, 0, 90f);
        }
        else if (angle > 22.5f && angle <= 67.5f && !_animator.GetBool("FBR"))
        {
            ResetFacingPosition();
            _animator.SetBool("FBR", true);
            attackCollider.transform.eulerAngles = new Vector3(0, 0, 225f);
        }
        else if (angle > 67.5f && angle <= 112.5f && !_animator.GetBool("FR"))
        {
            ResetFacingPosition();
            _animator.SetBool("FR", true);
            attackCollider.transform.eulerAngles = new Vector3(0, 0, 180f);
        }
        else if (angle > 112.5f && angle <= 157.5f && !_animator.GetBool("FFR"))
        {
            ResetFacingPosition();
            _animator.SetBool("FFR", true);
            attackCollider.transform.eulerAngles = new Vector3(0, 0, 135f);
        }


    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            Enemy enemy = collision.collider.gameObject.GetComponent<Enemy>();
            if (enemy.Alive())
            {
                GetHit(1, collision.transform.position);
            }
        }
    }
    public override void Die()
    {
        inputControls.Player.Disable();
        GameManager.Instance.deaths++;
        if (SaveSystem.activeSave != null)
            SaveSystem.SavePlayer(SaveSystem.activeSave.slot);
        _animator.SetBool("Dead", true);
        PlayerGUI.Instance.endMenu.SlideIn();
    }
    override public void GetHit(float damage, Vector3 sourceOfDamage)
    {
        if (!stats.IsDead())
        {
            KnockBack((Vector2)sourceOfDamage);
            float newHealth = stats.ReduceHearts(1);
            _animator.SetTrigger("Hit");
            if (newHealth == 0)
            {
                Die();
            }
        }
    }
    public void AttackEnemiesOnRange()
    {
        digestiveSystem.ConsumeCalories(stats.attackEnergyCost);
        if (enemiesInRange.Count == 0 && buildingsInRange.Count == 0)
            return;

        foreach (Enemy enemy in enemiesInRange)
        {
            if (stats != null)
            {
                enemy?.GetHit(stats.GetAttackDamage(), transform.position);
                GameObject spawnedEffect = ObjectPooler.Instance.SpawnFromPool("BobImpactEffect", enemy.transform.position, Quaternion.identity);
                ParticleSystem particleSystem = spawnedEffect.GetComponent<ParticleSystem>();
                particleSystem.Play();
            }
        }
        foreach (Building building in buildingsInRange)
        {
            if (stats != null)
            {
                building?.GetHit(stats.GetAttackDamage(), transform.position);
                GameObject spawnedEffect = ObjectPooler.Instance.SpawnFromPool("BobImpactEffect", building.transform.position, Quaternion.identity);
                ParticleSystem particleSystem = spawnedEffect.GetComponent<ParticleSystem>();
                particleSystem.Play();
            }
        }
    }
    public void MoveForward()
    {
        if (buildingsInRange.Count == 0 && enemiesInRange.Count == 0 && obstaclesInRange.Count == 0 && environmentAssetsInRange.Count == 0)
        {
            if (_animator.GetBool("FF"))
            {
                doTweenController.MoveToTarget(new Vector2(0, -1).normalized * distanceMoveForward + (Vector2)transform.position, true);
            } else if (_animator.GetBool("FFL"))
            {
                doTweenController.MoveToTarget(new Vector2(-1, -1).normalized * distanceMoveForward + (Vector2)transform.position, true);
            }
            else if (_animator.GetBool("FL"))
            {
                doTweenController.MoveToTarget(new Vector2(-1, 0).normalized * distanceMoveForward + (Vector2)transform.position, true);
            }
            else if (_animator.GetBool("FBL"))
            {
                doTweenController.MoveToTarget(new Vector2(-1, 1).normalized * distanceMoveForward + (Vector2)transform.position, true);
            }
            else if (_animator.GetBool("FB"))
            {
                doTweenController.MoveToTarget(new Vector2(0, 1).normalized * distanceMoveForward + (Vector2)transform.position, true);
            }
            else if (_animator.GetBool("FBR"))
            {
                doTweenController.MoveToTarget(new Vector2(1, 1).normalized * distanceMoveForward + (Vector2)transform.position, true);
            }
            else if (_animator.GetBool("FR"))
            {
                doTweenController.MoveToTarget(new Vector2(1, 0).normalized * distanceMoveForward + (Vector2)transform.position, true);
            }
            else if (_animator.GetBool("FFR"))
            {
                doTweenController.MoveToTarget(new Vector2(1, -1).normalized * distanceMoveForward + (Vector2)transform.position, true);
            }
        }
    }
    public float CaloriesLeft() => digestiveSystem.CaloriesLeft();
    public float AddCalories(float caloriesToAdd) => digestiveSystem.AddCalories(caloriesToAdd);
    public float ReduceCalories(float caloriesToReduce) => digestiveSystem.ConsumeCalories(caloriesToReduce);
}
