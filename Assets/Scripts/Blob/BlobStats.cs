using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobStats : MonoBehaviour
{
    public int defaultMaxHearts = 3;
    public int attackEnergyCost;
    public int rangeEnergyCost;
    public int hardenningEnergyCost;

    [Header("States")]
    public bool beingHit = false;
    public bool isHardenned = false;
    public bool blocking = false;
    public bool perfectBlocking = false;
    public bool canAttack = false;
    public bool canShoot = false;
    public bool canEat = true;
    public bool eating = false;
    public float defaultAttackDamage = 50f;

    public event Action OnAttackModiffierProcess;
    public event Action<AttackModifier> OnAttackModiffierAdded;

    private float attackDamage;
    private float hearts;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private DigestiveSystem _digestiveSystem;

    private List<AttackModifier> activeAttackModifiers = new List<AttackModifier>();
    private List<StatsModifier> activeStatsModifiers = new List<StatsModifier>();
    private List<AlteredState> activeAlteredStatesModifiers = new List<AlteredState>();
    private void Start()
    {
        Setup();
    }
    public void Setup() 
    {
        attackDamage = defaultAttackDamage;
        hearts = defaultMaxHearts;
        _playerController = gameObject.GetComponent<PlayerController>();
        _digestiveSystem = gameObject.GetComponent<DigestiveSystem>();
    }
    public float ReduceHearts(float reduceHearts)
    {
        hearts -= reduceHearts;
        PlayerGUI.Instance.ReduceHealth(reduceHearts);
        return hearts;
    }
    public void AddHearts(float heartsToAdd)
    {
        hearts += heartsToAdd;
        PlayerGUI.Instance.AddHealth(heartsToAdd);
    }
    public bool IsDead()
    {
        return hearts == 0;
    }
    public float GetAttackDamage()
    {
        return attackDamage;    
    }
    public void BlockAnimationEvent()
    {
        blocking = true;
    }
    public void StopBlockingAnimationEvent()
    {
        blocking = false;
    }
    public void PerfectBlockAnimationEvent()
    {
        perfectBlocking = true;
    }
    public void StopPerfectBlockAnimationEvent()
    {
        perfectBlocking = false;
    }
    public float HeartsLeft() => hearts;
    public void AddAttackModifier(AttackModifier modifier)
    {
        Debug.Log($"TRYING TO ADD NEW MODIFIER: {modifier.type} {modifier.duration} {modifier.enablesAttack}");
        if (activeAttackModifiers.Exists( attackModifier => attackModifier.type == modifier.type ))
        {

            Debug.Log($"ALREADY EXISTS... ADDING TIME");
            AttackModifier activeModifier = activeAttackModifiers.Find(attackModifier => attackModifier.type == modifier.type);
            activeModifier.duration += modifier.duration;
            OnAttackModiffierAdded?.Invoke(activeModifier);
        }
        else
        {
            Debug.Log($"DOESNT EXIST ON ACTIVE MODIFIERS... ADDING TO LIST");
            AttackModifier newModifier = ScriptableObject.Instantiate(modifier);
            Debug.Log($"NEW MODIFIER TO ADD: {newModifier.type} {newModifier.duration} {newModifier.enablesAttack}");
            newModifier.type = modifier.type;
            newModifier.duration = modifier.duration;
            newModifier.enablesAttack = modifier.enablesAttack;
            Debug.Log($"NEW MODIFIER AFTER INITIALIZE: {newModifier.type} {newModifier.duration} {newModifier.enablesAttack}");
            activeAttackModifiers.Add(newModifier);
            Debug.Log($"NEW MODIFIER ADDED!");
            Debug.Log($"INVOKING ON ATTACK MODIFFIER ADDED...");
            OnAttackModiffierAdded?.Invoke(newModifier);
            StartCoroutine(ProcessAttackModifier(newModifier));
        }
    }
    private IEnumerator ProcessAttackModifier(AttackModifier modifier)
    {
        switch (modifier.type)
        {
            case AttackModifierType.MELEE:
                if (modifier.enablesAttack)
                {
                    canAttack = true;
                    PlayerGUI.Instance.attackDisplay.Show();
                }
                break;
            case AttackModifierType.RANGED:
                if (modifier.enablesAttack)
                {
                    canShoot = true;
                    PlayerGUI.Instance.rangedDisplay.Show();
                }
                break;
        }
        while (modifier.duration > 0)
        {
            float timeToWait = 0.2f;
            yield return new WaitForSeconds(timeToWait);
            modifier.duration -= timeToWait;
            modifier.duration = Mathf.Clamp(modifier.duration, 0f, Mathf.Infinity);
            OnAttackModiffierProcess?.Invoke();
        }
        switch (modifier.type)
        {
            case AttackModifierType.MELEE:
                if (modifier.enablesAttack)
                    canAttack = false;
                break;
            case AttackModifierType.RANGED:
                if (modifier.enablesAttack)
                    canShoot = false;
                break;
        }
        activeAttackModifiers.Remove(modifier);
    }
    private IEnumerator ProcessStatModifier(StatsModifier modifier)
    {
        while (modifier.duration > 0)
        {
            switch (modifier.type)
            {
                case StatsModifierType.STOMACH:
                    //Add stomach size
                    break;
                case StatsModifierType.HEART:
                    //Add max hearts
                    break;
                case StatsModifierType.DAMAGE:
                    //Change damage
                    break;
            }
            float timeToWait = modifier.duration;
            yield return new WaitForSeconds(modifier.duration);
            modifier.duration -= timeToWait;
        }
    }
    private IEnumerator ProcessAlteredStateModifier(AlteredState modifier)
    {
        while (modifier.numberOfTicks > 0)
        {
            float timeToWait = 1 / modifier.ticksPerSecond;
            yield return new WaitForSeconds(timeToWait);
            _playerController.GetHit(1f, transform.position);
            modifier.numberOfTicks -= 1;
        }
    }
}
