using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _animator = gameObject.GetComponent<Animator>();
    }

    public void Fill()
    {
        _animator.SetTrigger("Fill");
    }

    public void Deplete()
    {
        _animator.SetTrigger("Deplete");
    }
    
}
