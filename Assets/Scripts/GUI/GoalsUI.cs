using System;
using UnityEngine;
using TMPro;

public class GoalsUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI campsLeftText;
    [SerializeField] private TextMeshProUGUI bossText;

    private void Start()
    {
        Refresh();
    }
    public void Refresh()
    {
        campsLeftText.SetText($"{GameManager.Instance.GetCampsLeft().Count}/{GameManager.Instance.GetCamps().Count}");
        Enemy boss = GameManager.Instance.GetBoss();
        if (boss)
        {
            bossText.SetText(boss.Alive()?"Vivo":"Muerto");
        } else
        {
            bossText.SetText("Oculto");
        }
    }
}
