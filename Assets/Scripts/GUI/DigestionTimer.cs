using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Digestion Timer", menuName = "Timers/Digestion")]
public class DigestionTimer : ScriptableObject
{
    public new string name;
    [Range(0, 1)]
    public float percentageLeft = 1f;
    public Sprite icon; 
}
