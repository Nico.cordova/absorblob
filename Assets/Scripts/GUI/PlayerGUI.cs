using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PlayerGUI : GenericDestroyableSingletonClass<PlayerGUI>
{
    public EnergyBar energy;
    public InputMaster inputControls;

    [Header("Prefab")]
    public GameObject heartPrefab;
    public GameObject heartContainer;
    public GameObject digestionTimerDisplay;
    public GameObject digestionTimersContainer;

    [Header("UI References")]
    [SerializeField] private GoalsUI goals;
    public PauseMenu pauseMenu;
    public EndMenu endMenu;
    public AttackTimerDisplay attackDisplay;
    public RangedTimerDisplay rangedDisplay;

    [Header("EventSystem References")]
    public GameObject pauseMenuFirstButton;
    public GameObject endMenuFirstButton;

    [Header("DEBUGGING")]
    [SerializeField] private Queue<DigestionTimerDisplay> usedTimers = new Queue<DigestionTimerDisplay>();
    [SerializeField] private Queue<DigestionTimerDisplay> unusedTimers = new Queue<DigestionTimerDisplay>();

    private List<Heart> fullHearts = new List<Heart>();
    private List<Heart> emptyHearts = new List<Heart>();
    
    // Start is called before the first frame update
    void Start()
    {
        BlobStats playerStats = GameManager.Instance.mainPlayer.stats;
        DigestiveSystem playerDigestiveSystem = GameManager.Instance.mainPlayer.GetComponent<DigestiveSystem>();
        while (fullHearts.Count < playerStats.defaultMaxHearts)
        {
            GameObject heartGO = Instantiate<GameObject>(heartPrefab, heartContainer.transform);
            Heart heart = heartGO.GetComponent<Heart>();
            Image heartImage = heartGO.GetComponent<Image>();
            heartImage.SetNativeSize();
            heart.Fill();
            fullHearts.Add(heart);
        }
        
        while (unusedTimers.Count < playerDigestiveSystem.defaultStomachSize) {
            GameObject timerGO = Instantiate<GameObject>(digestionTimerDisplay, digestionTimersContainer.transform);
            DigestionTimerDisplay timer = timerGO.GetComponent<DigestionTimerDisplay>();
            timer.Hide();
            unusedTimers.Enqueue(timer);
        }

        Setup();
    }
    public void Setup()
    {
        BlobStats playerStats = GameManager.Instance.mainPlayer.stats;
        DigestiveSystem playerDigestiveSystem = GameManager.Instance.mainPlayer.GetComponent<DigestiveSystem>();
        goals = GetComponentInChildren<GoalsUI>();
        if (attackDisplay == null)
        {
            attackDisplay = GetComponent<AttackTimerDisplay>();
        }
        if (rangedDisplay == null)
        {
            rangedDisplay = GetComponent<RangedTimerDisplay>();
        }
        if (usedTimers.Count > 0)
        {
            while (usedTimers.Count > 0)
            {
                DigestionTimerDisplay timer = usedTimers.Dequeue();
                DigestionTimerCleanup(timer);
                
            }
        }

        if (emptyHearts.Count > 0)
        {
            while (emptyHearts.Count > 0)
            {
                Heart heart = emptyHearts.First();
                heart.Fill();
                emptyHearts.Remove(heart);
                fullHearts.Add(heart);
            }
        }

        playerDigestiveSystem.OnCaloriesChange += UpdateEnergySlider;
        energy.slider.maxValue = playerDigestiveSystem.defaultMaxCalories;
        energy.expectedEnergy = playerDigestiveSystem.defaultInitialCalories;
        energy.slider.value = playerDigestiveSystem.defaultInitialCalories;

        energy.backSlider.maxValue = playerDigestiveSystem.defaultMaxCalories;
        energy.backSlider.value = playerDigestiveSystem.defaultInitialCalories;
    }
    private void OnDestroy()
    {
        if (GetComponent<GameManager>())
        {
            DigestiveSystem playerDigestiveSystem = GameManager.Instance.mainPlayer.GetComponent<DigestiveSystem>();
            if (playerDigestiveSystem != null)
                playerDigestiveSystem.OnCaloriesChange -= UpdateEnergySlider;
        }
    }
    public void ReduceHealth(float healthToReduce)
    {
        for (int i =0; i<healthToReduce; i++)
        {
            if (fullHearts.Count == 0)
                return;
            Heart fullHeart = fullHearts.Last();
            fullHeart.Deplete();
            fullHearts.Remove(fullHeart);
            emptyHearts.Add(fullHeart);
        }
    }
    public DigestionTimerDisplay GetDigestionTimerToUse()
    {
        if (unusedTimers.Count > 0)
        {
            DigestionTimerDisplay displayToUse = unusedTimers.Dequeue();
            displayToUse.gameObject.SetActive(true);
            usedTimers.Enqueue(displayToUse);
            return displayToUse;
        } else
        {
            return null;
        }
    }
    public void DigestionTimerCleanup(DigestionTimerDisplay digestionTimer)
    {
        digestionTimer.Hide();
        unusedTimers.Enqueue(digestionTimer);
    }
    public void AttackTimerCleanup(AttackTimerDisplay attackTimer)
    {
        attackTimer.Hide();
    }
    public void AddHealth(float healthToAdd)
    {
        for (int i = 0; i < healthToAdd; i++)
        {
            if (emptyHearts.Count == 0)
                return;
            Heart emptyHeart = emptyHearts.Last();
            emptyHeart.Fill();
            emptyHearts.Remove(emptyHeart);
            fullHearts.Add(emptyHeart);
        }
    }
    public void UpdateGoals()
    {
        if (goals)
            goals.Refresh();
    }
    public void UpdateEnergySlider()
    {
        energy.Refresh();
    }
}
