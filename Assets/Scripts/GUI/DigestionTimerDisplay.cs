using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DigestionTimerDisplay : MonoBehaviour
{
    public Image progress;
    private Consumable consumable;
    [SerializeField]
    private Image iconRenderer;
    private void Awake()
    {
        Hide();
    }
    private void Update()
    {
        if (consumable.timerInfo.percentageLeft >= 0)
        {
            progress.fillAmount = consumable.timerInfo.percentageLeft;
            if (consumable.timerInfo.percentageLeft == 0)
            {
                PlayerGUI.Instance.DigestionTimerCleanup(this);
            }
        }
    }
    public Consumable GetConsumable()
    {
        return consumable;
    }
    public void InitFromConsumable(Consumable newConsumable)
    {
        consumable = newConsumable;
        iconRenderer.sprite = newConsumable.timerInfo.icon;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
}
