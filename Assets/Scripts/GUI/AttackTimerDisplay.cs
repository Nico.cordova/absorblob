using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackTimerDisplay : MonoBehaviour
{
    public Image progress;
    public AttackModifier modifier; 
    public float lastMax;
    private void Start()
    {
        GameManager.Instance.mainPlayer.stats.OnAttackModiffierProcess += UpdateProgress;
        GameManager.Instance.mainPlayer.stats.OnAttackModiffierAdded += UpdateFromAttackModifier;
        Hide();
    }
    public void UpdateFromAttackModifier(AttackModifier newModifier)
    {
        if (newModifier.type == AttackModifierType.MELEE)
        {
            modifier = newModifier;
            lastMax = newModifier.duration;
        }
    }
    public void UpdateProgress()
    {
        if (modifier != null)
        {
            progress.fillAmount = modifier.duration/lastMax;
            if (modifier.duration / lastMax <= 0)
            {
                Hide();
            }
        }
    }
    public void Hide()
    {
        modifier = null;
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
}
