using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatIndicator : GenericDestroyableSingletonClass<EatIndicator>
{
    [SerializeField]
    private SpriteRenderer iconRenderer;
    [SerializeField]
    private SpriteRenderer indicatorRenderer;

    public void MoveTo(Vector2 position)
    {
        transform.position = position;
    }
    public void Hide() {
        iconRenderer.enabled = false;
        indicatorRenderer.enabled = false;
    }
    public void Show() {
        iconRenderer.enabled = true;
        indicatorRenderer.enabled = true;
    }
}
