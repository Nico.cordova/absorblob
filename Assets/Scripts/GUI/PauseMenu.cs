using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PauseMenu : TabScreen
{
    [SerializeField] private GameObject panel;
    private Image background;
    private void Awake()
    {
        background = gameObject.GetComponent<Image>();
    }
    public override void Show()
    {
        background.enabled = true;
        panel.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(PlayerGUI.Instance.pauseMenuFirstButton);
    }
    public void OnClickContinue() {
        Time.timeScale = 1f;
        PlayerGUI.Instance.pauseMenu.SlideOut();
    }
    public void OnClickRestart() {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        OnClickContinue();
        GameManager.Instance.Setup();
        GameManager.Instance.mainPlayer.Setup();
        PlayerGUI.Instance.Setup();
        ObjectPooler.Instance.Setup();
    }
    public void OnClickExit()
    {
        SaveSystem.SavePlayer(SaveSystem.activeSave.slot);
        Time.timeScale = 1f;
        SlideOut();
        SceneManager.LoadScene("TitleScreen");
    }
    public override void Hide()
    {
        background.enabled = false;
        panel.SetActive(false);
    }


    public override void OnShowAnimationEnd()
    {
        Time.timeScale = 0f;
    }

    public override void OnShowAnimationStart()
    {
    }

    public override void OnHideAnimationEnd()
    {
    }

    public override void OnHideAnimationStart()
    {
    }
}
