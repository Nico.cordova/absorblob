using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamagePopUp : MonoBehaviour
{
    public float defaultFadeTime;
    public float defaultTimeOnScreen = 0.1f;
    public TextMeshPro damage;
    [SerializeField] private float increaseScaleAmount = 1f;
    [SerializeField] private float decreaseScaleAmount = 1f;
    private Color textColor;
    private float fadeTime;
    private float timeOnScreen;
    private Vector3 moveDirection = new Vector3(0.7f,1);
    private Vector3 originalScale = Vector3.back;
    private void Start()
    {
        if (originalScale == Vector3.back)
        {
            originalScale = transform.localScale;
        }
    }
    public void Initialize(float number)
    {
        gameObject.SetActive(true);
        damage.text = number.ToString();
        fadeTime = defaultFadeTime;
        timeOnScreen = defaultTimeOnScreen;
        textColor = damage.color;
    }
    public void Update()
    {
        transform.position += moveDirection * Time.deltaTime;
        moveDirection -= moveDirection * 8f * Time.deltaTime;
        timeOnScreen -= Time.deltaTime;
        if (timeOnScreen > defaultTimeOnScreen * .5f)
        {
            transform.localScale += Vector3.one * increaseScaleAmount * Time.deltaTime; 
        } else
        {

            transform.localScale -= Vector3.one * decreaseScaleAmount * Time.deltaTime;
        }
        if (timeOnScreen < 0)
        {
            float dissapearSpeed = 3f;
            textColor.a -= dissapearSpeed * Time.deltaTime;
            damage.color = textColor;
            if (damage.color.a < 0)
            {
                textColor.a = 1;
                damage.color = textColor;
                transform.localScale = originalScale;
                gameObject.SetActive(false);
            }
        }
    }
}
