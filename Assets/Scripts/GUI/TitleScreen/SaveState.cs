using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveState : MonoBehaviour, IPointerEnterHandler, ISelectHandler, IDeselectHandler
{
    [SerializeField] private TMPro.TextMeshProUGUI deathsText;
    [SerializeField] private TMPro.TextMeshProUGUI timeAliveText;
    [SerializeField] private TMPro.TextMeshProUGUI slotText;
    [SerializeField] private Animator animator;
    [SerializeField] private Image playerIcon;
    [SerializeField] private List<Image> dataImages = new List<Image>();
    [SerializeField] private SaveSlot saveSlot;
    [SerializeField] private Button slotButton;
    private void Start()
    {
        if (slotButton == null)
            slotButton = gameObject.GetComponent<Button>();
        if (animator == null)
            animator = gameObject.GetComponentInChildren<Animator>();
        if (playerIcon)
        {
            playerIcon.material = new Material(playerIcon.material);
        }
        if (dataImages.Count > 0)
        {
            foreach (Image image in dataImages)
            {
                image.material = new Material(image.material);
            }
        }
    }
    public SaveSlot SaveData()
    {
        return saveSlot;
    }
    public void OnSelectSaveState()
    {
        SaveSystem.activeSave = saveSlot;
        //CHANGE TO LOAD SCENE DEPENDING ON COMPLETED LEVELS
        SceneManager.LoadScene("InitialLevel");
    }
    public void SetupFromSave(SaveSlot save)
    {
        saveSlot = save;
        if (saveSlot.savedData != null)
        {
            // Setting up icon
            if (saveSlot.savedData.heartsLeft == 0)
            {
                animator.Play("BobDie");
            }

            // Setting up Islands
            for (int i = 0; i < dataImages.Count; i++)
            {
                Image image = dataImages[i];
                switch (i)
                {
                    case 0:
                        if (saveSlot.savedData.completedLevels >= 1)
                        {
                            image.material.color = MainMenu.Instance.HighlightColor;
                        } else
                        {
                            image.material.color = MainMenu.Instance.NotHighlightedColor;
                        }
                        break;
                    case 2:
                        if (saveSlot.savedData.completedLevels >= 2)
                        {
                            dataImages[i-1].material.color = MainMenu.Instance.HighlightColor;
                            image.material.color = MainMenu.Instance.HighlightColor;
                        }
                        else
                        {
                            dataImages[i - 1].material.color = MainMenu.Instance.NotHighlightedColor;
                            image.material.color = MainMenu.Instance.NotHighlightedColor;
                        }
                        break;
                    case 4:
                        if (saveSlot.savedData.completedLevels >= 3)
                        {
                            dataImages[i - 1].material.color = MainMenu.Instance.HighlightColor;
                            image.material.color = MainMenu.Instance.HighlightColor;
                        }
                        else
                        {
                            dataImages[i - 1].material.color = MainMenu.Instance.NotHighlightedColor;
                            image.material.color = MainMenu.Instance.NotHighlightedColor;
                        }
                        break;
                    case 6:
                        if (saveSlot.savedData.completedLevels >= 4)
                        {
                            dataImages[i - 1].material.color = MainMenu.Instance.HighlightColor;
                            image.material.color = MainMenu.Instance.HighlightColor;
                        }
                        else
                        {
                            dataImages[i - 1].material.color = MainMenu.Instance.NotHighlightedColor;
                            image.material.color = MainMenu.Instance.NotHighlightedColor;
                        }
                        break;
                    case 8:
                        if (save.savedData.completedLevels >= 5)
                        {
                            dataImages[i - 1].material.color = MainMenu.Instance.HighlightColor;
                            image.material.color = MainMenu.Instance.HighlightColor;
                        }
                        else
                        {
                            dataImages[i - 1].material.color = MainMenu.Instance.NotHighlightedColor;
                            image.material.color = MainMenu.Instance.NotHighlightedColor;
                        }
                        break;
                }
            }

            // Setting up Time Alive
            timeAliveText.text = $"{(saveSlot.savedData.secondsAlive/60).ToString("00")}:{(saveSlot.savedData.secondsAlive % 60).ToString("00")}";
            // Setting up deaths
            deathsText.text = $"DEATHS {saveSlot.savedData.deaths}";
            // Setting up Slot 
            slotText.text = $"SAVE {saveSlot.slot}";
        }
        else
        {
            for (int i = 0; i < dataImages.Count; i++)
            {
                Image image = dataImages[i];
                image.material.color = MainMenu.Instance.NotHighlightedColor;
            }
            timeAliveText.text = $"00:00";
            // Setting up deaths
            deathsText.text = $"DEATHS 0";
            // Setting up Slot 
            slotText.text = $"SAVE {save.slot}";
        }
    }
    public void OnSelect(BaseEventData eventData)
    {
        Highlight();
    }
    public void OnDeselect(BaseEventData eventData)
    {
        StopHighlight();
    }
    public void OnPointer()
    {
    }
    public void OnPointerEnter(PointerEventData data)
    {
    }
    private void Highlight()
    {
        animator.SetBool("Highlighted", true);
        
        playerIcon.material.color = MainMenu.Instance.HighlightColor;
    }
    private void StopHighlight()
    {
        animator.SetBool("Highlighted", false);
        foreach (Image image in dataImages)
        {
            image.material.color = MainMenu.Instance.NotHighlightedColor;
        }
        playerIcon.material.color = MainMenu.Instance.NotHighlightedColor;
    }
}
