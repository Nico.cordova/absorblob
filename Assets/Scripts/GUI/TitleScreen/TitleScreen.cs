using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class TitleScreen : TabScreen
{
    [SerializeField] private GameObject title;
    [SerializeField] private GameObject buttonsPanel;
    // Start is called before the first frame update
    public void GoToSavesScreen()
    {
        SlideOut();
        MainMenu.Instance.saveScreen.SlideIn();
    }
    public void CloseGame()
    {

    }
    public override void Show()
    {
        title.SetActive(true);
        buttonsPanel.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(MainMenu.Instance.titleFirstButton);
    }
    public override void Hide()
    {
        title.SetActive(false);
        buttonsPanel.SetActive(false);
    }

    public override void OnShowAnimationEnd()
    {
    }

    public override void OnShowAnimationStart()
    {
    }

    public override void OnHideAnimationEnd()
    {
    }

    public override void OnHideAnimationStart()
    {
    }
}
