using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : GenericDestroyableSingletonClass<MainMenu>
{
    public SaveStateSelectScreen saveScreen;
    public TitleScreen titleScreen;
    public Color HighlightColor;
    public Color NotHighlightedColor;
    public GameObject titleFirstButton, savesSelectFirstButton;
}
