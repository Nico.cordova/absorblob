using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class SaveStateSelectScreen : TabScreen
{
    // Start is called before the first frame update
    [SerializeField] private GameObject savesPanel;
    [SerializeField] private List<SaveState> saveStates = new List<SaveState>();

    
    [Header("Prefabs")]
    [SerializeField] private GameObject saveSlotPrefab;
    [SerializeField] private GameObject emptySaveSlotPrefab;
    private void Start()
    {
        int index = 1;
        while (index <= 3)
        {
            PlayerSaveData savedData = SaveSystem.LoadPlayer(index);
            saveStates[index-1].SetupFromSave(new SaveSlot(index,savedData));
            index++;
        }
    }
    public void BackToTitleScreen()
    {
        SlideOut();
        MainMenu.Instance.titleScreen.SlideIn();
    }
    public void GoToSettings()
    {

    }
    
    public override void Show()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(MainMenu.Instance.savesSelectFirstButton);
        savesPanel.SetActive(true);

    }
    public override void Hide()
    {
        savesPanel.SetActive(false);
    }

    public override void OnShowAnimationEnd()
    {
    }

    public override void OnShowAnimationStart()
    {
    }

    public override void OnHideAnimationEnd()
    {
    }

    public override void OnHideAnimationStart()
    {
    }
}
