using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CreateAssetMenu(fileName = "New Level Info", menuName = "Levels/LevelInfo")]
public class LevelInfo : ScriptableObject
{
    public string title;
    public Transform bossSpawnPosition;
    [Range(1,10)]
    public int camps;
    public string scene;
    public GameObject boss;
    public List<GameObject> enemies;

}
