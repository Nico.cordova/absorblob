using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WailzardAnimatorEvents : MonoBehaviour
{
    Shooting _shooting;
    Enemy _enemy;
    [SerializeField]
    LineTelegraph _lineTelegraph;
    void Awake()
    {
        _enemy = gameObject.GetComponentInParent<Enemy>();
        _shooting = gameObject.GetComponentInParent<Shooting>();
        _lineTelegraph = gameObject.GetComponentInChildren<LineTelegraph>();
    }

    public void Shoot()
    {
        _shooting.Shoot(_enemy.GetTarget());
    }
    public void AnimateTelegraph()
    {
        _lineTelegraph.Animate();
    }
    public void StopAnimatingTelegraph()
    {
        _lineTelegraph.StopAnimating();
    }
}
