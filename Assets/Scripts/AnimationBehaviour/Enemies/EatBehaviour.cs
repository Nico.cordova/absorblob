using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class EatBehaviour : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Enemy enemyController = animator.gameObject.GetComponentInParent<Enemy>();
        Consumable nearestConsumable = enemyController.GetNearestConsumable();
        if (nearestConsumable)
        {
            DigestiveSystem digestiveSystem = animator.gameObject.GetComponentInParent<DigestiveSystem>();
            Debug.Log($"[{animator.GetComponentInParent<Enemy>().name}] TRYING TO EAT NEAREST CONSUMABLE: {nearestConsumable.name}");
            digestiveSystem.EatConsumable(nearestConsumable);
        } else if (enemyController != null)
        {
            enemyController.MoveToNearestConsumable();
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
