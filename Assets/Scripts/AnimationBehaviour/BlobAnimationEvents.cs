using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobAnimationEvents : MonoBehaviour
{
    public void EatConsumableEvent()
    {
        PlayerController _player = gameObject.GetComponent<PlayerController>();
        if (_player != null)
        {
            DigestiveSystem digestiveSystem = gameObject.GetComponent<DigestiveSystem>();
            Consumable nearestConsumable = digestiveSystem.GetNearestConsumable();
            if (nearestConsumable && digestiveSystem.CanEat(nearestConsumable))
            {
                nearestConsumable.canBeEaten = false;
                digestiveSystem.EatConsumable(nearestConsumable);
                _player.stats.AddHearts(nearestConsumable.heartsWorth);
            }
        }
    }
    public void DisableInputs()
    {
        PlayerController _player = gameObject.GetComponent<PlayerController>();
        if (_player != null)
        {

            _player.inputControls.Disable();
        }
    }
    public void EnableInputs()
    {
        PlayerController _player = gameObject.GetComponent<PlayerController>();
        if (_player != null)
        {
            _player.inputControls.Enable();
        }
    }
}
