using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBehaviour : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PlayerController _player = animator.gameObject.GetComponent<PlayerController>();
        if (_player != null)
        {
            _player.inputControls.Disable();
            _player.attacksDetected = 0;
            animator.gameObject.GetComponent<PlayerController>().stats.beingHit = true;
        }
        Enemy _enemy = animator.gameObject.GetComponentInParent<Enemy>();
        if (_enemy != null)
        {
            _enemy.pathfinder.enabled = false;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        PlayerController _player = animator.gameObject.GetComponent<PlayerController>();
        if (_player != null)
        {
            _player.inputControls.Enable();
            _player.ResetVelocity();
            _player.stats.beingHit = false;
        }
        Enemy _enemy = animator.gameObject.GetComponentInParent<Enemy>();
        if (_enemy != null)
        {
            _enemy.pathfinder.enabled = true;
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
