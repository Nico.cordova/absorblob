using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    public Slider slider;
    public Slider backSlider;

    [SerializeField] [Range(0,5)] private float delayTime;
    [SerializeField] [Range(0,5)] private float lerpTime;
    public float expectedEnergy;
    private float actualEnergy = 0f;

    private float sleepTime;
    private void Start()
    {
        if (slider == null)
            slider = gameObject.GetComponent<Slider>();
        if (backSlider == null)
            backSlider = gameObject.GetComponentInChildren<Slider>();
        if (GameManager.Instance.mainPlayer != null)
            actualEnergy = GameManager.Instance.mainPlayer.CaloriesLeft();
        sleepTime = delayTime;
    }
    void Update()
    {
        sleepTime -= Time.deltaTime;
        sleepTime = Mathf.Clamp(sleepTime, 0f, Mathf.Infinity);
        if (sleepTime != 0f)
            return;

        if (expectedEnergy != actualEnergy)
        {
            if (actualEnergy == 0)
            {
                actualEnergy = expectedEnergy;
                backSlider.value = actualEnergy;
            }
            else 
            {
                actualEnergy = Mathf.Lerp(actualEnergy, expectedEnergy, lerpTime * Time.deltaTime);
                backSlider.value = actualEnergy;
            }
        }
    }
    public void Refresh()
    {
        sleepTime = delayTime;
        expectedEnergy = GameManager.Instance.mainPlayer.CaloriesLeft();
        slider.value = expectedEnergy;
    }
}
