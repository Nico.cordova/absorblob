using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndMenu : TabScreen
{
    [SerializeField] private TMPro.TextMeshProUGUI title;
    [SerializeField] private GameObject panel;
    private Image background;
    private void Awake()
    {
        background = gameObject.GetComponent<Image>();
        if (title == null)
        {
            title = gameObject.GetComponent<TMPro.TextMeshProUGUI>();
        }
    }
    public override void Show()
    {
        if (GameManager.Instance.mainPlayer.stats.HeartsLeft() == 0)
        {
            title.text = "¡Perdiste!";
        } else
        {
            title.text = "¡Ganaste!";
        }
        panel.SetActive(true);
        background.enabled = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(PlayerGUI.Instance.endMenuFirstButton);
    }
    public override void Hide()
    {
        background.enabled = false;
        panel.SetActive(false);
    }
    public void OnClickRestart()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        Time.timeScale = 1f;
        SlideOut();
        GameManager.Instance.Setup();
        GameManager.Instance.mainPlayer.Setup();
        PlayerGUI.Instance.Setup();
        ObjectPooler.Instance.Setup();
    }
    public void OnClickExit()
    {
        SaveSystem.SavePlayer(SaveSystem.activeSave.slot);
        Time.timeScale = 1f;
        SlideOut();
        SceneManager.LoadScene("TitleScreen");
    }
    public override void OnShowAnimationEnd()
    {
        Time.timeScale = 0f;
    }

    public override void OnShowAnimationStart()
    {
    }

    public override void OnHideAnimationEnd()
    {
    }

    public override void OnHideAnimationStart()
    {
        Time.timeScale = 1f;
    }
}
