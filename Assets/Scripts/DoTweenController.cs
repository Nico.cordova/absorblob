using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoTweenController : MonoBehaviour
{
    [SerializeField]
    private LayerMask collidersLayer;
    [Range(0.1f, 10.0f), SerializeField]
    private float _moveDuration = 1.0f;
    
    [Range(0f, 1f), SerializeField]
    private float _moveForwardDuration = 1.0f;
    
    [SerializeField]
    private Ease _moveEase = Ease.Linear;
    
    [SerializeField]
    private DoTweenType _doTweenType = DoTweenType.MovementOneWay;

    private enum DoTweenType
    {
        MovementOneWay,
    }
    public void MoveToTarget(Vector2 targetLocation, bool moveForward=false) {
        RaycastHit2D hit = Physics2D.Linecast(transform.position, targetLocation, collidersLayer);
        if (hit)
        {
            Vector2 raycastDir = hit.point - (Vector2)transform.position;
            RaycastHit2D hit2 = Physics2D.Raycast(hit.point, raycastDir, collidersLayer);
            targetLocation = hit.point;
        }
        if (_doTweenType == DoTweenType.MovementOneWay)
        {
            if (targetLocation == Vector2.zero)
            {
                targetLocation = transform.position;
            }
            if (moveForward)
            {
                transform.DOMove(targetLocation, _moveForwardDuration).SetEase(_moveEase);
            } else
            {
                transform.DOMove(targetLocation, _moveDuration).SetEase(_moveEase);
            }
        }
    }
}
