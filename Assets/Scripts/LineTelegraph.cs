using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineTelegraph : MonoBehaviour
{
    public float animationDuration;
    [SerializeField]
    private SpriteRenderer _spriteRenderer;
    [SerializeField]
    private Enemy _enemy;
    private void Start()
    {
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _enemy = gameObject.GetComponentInParent<Enemy>();
    }
    private IEnumerator LerpSize()
    {
        float _timeAnimating = 0f;

        while (_timeAnimating < animationDuration)
        {
            Vector2 direction = _enemy.GetTarget().transform.position - transform.position;
            float distance = direction.magnitude;
            float angle = Vector2.SignedAngle(direction, Vector2.left);
            
            transform.rotation = Quaternion.Euler(0, 0, -angle);
            float time = _timeAnimating / animationDuration;
            time = time * time * (3f - 2f * time);
            _spriteRenderer.size = new Vector2(Mathf.Lerp(0, distance, time), _spriteRenderer.size.y);
            _timeAnimating += Time.deltaTime;
            yield return null;
        }
        Vector2 lastDirection = _enemy.GetTarget().transform.position - transform.position;
        float lastDistance = lastDirection.magnitude;
        float lastTime = _timeAnimating / animationDuration;
        lastTime = lastTime * lastTime * (3f - 2f * lastTime);
        float lastAngle = Vector2.SignedAngle(lastDirection, Vector2.left);
        transform.rotation = Quaternion.Euler(0, 0, -lastAngle);
        _spriteRenderer.size = new Vector2(Mathf.Lerp(0, lastDistance, lastTime), _spriteRenderer.size.y);
    }
    public void Animate()
    {
        _spriteRenderer.enabled = true;
        StartCoroutine(LerpSize());
    }
    public void StopAnimating()
    {
        _spriteRenderer.size = new Vector2(0, _spriteRenderer.size.y);
        _spriteRenderer.enabled = false;
    }
}
