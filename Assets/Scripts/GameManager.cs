using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
public class GameManager : GenericSingletonClass<GameManager>
{
    [Header("Global Access")]
    public PlayerGUI playerGUI;
    public PlayerController mainPlayer;
    public Material highlightMaterial;
    public Material defaultLitMaterial;
    public GameObject damagePopUp;
    public List<LevelInfo> levels;
    [SerializeField] private SaveSlot saveSlotInUse;

    [Header("Game goals assets")]
    [SerializeField] private List<Camp> camps = new List<Camp>();
    [SerializeField] private Enemy boss = null;

    [Header("Monitoring")]
    public int deaths = 0;
    public float timeAlive;
    [SerializeField] private LevelInfo activeLevel;
    [SerializeField] private List<LevelInfo> uncompletedLevels;
    [SerializeField] private List<LevelInfo> completedLevels;
    
    private void Start()
    {
        Setup();
    }
    private void Update()
    {
        if (mainPlayer != null && !mainPlayer.stats.IsDead())
        {
            timeAlive += Time.deltaTime;
        }
    }
    public void Setup()
    {
        timeAlive = 0f;
        uncompletedLevels = levels;
        mainPlayer = GameObject.FindObjectOfType<PlayerController>();
        mainPlayer.Setup();
        saveSlotInUse = SaveSystem.activeSave;
        if (saveSlotInUse != null && saveSlotInUse.savedData != null && saveSlotInUse.savedData.heartsLeft > 0)
            saveSlotInUse.savedData.LoadData();
        else if (saveSlotInUse != null && saveSlotInUse.savedData != null && saveSlotInUse.savedData.heartsLeft == 0)
            saveSlotInUse.savedData.LoadData(true);

        if (activeLevel != null)
        {
            SetUpFromLevel(activeLevel);
        }
        else
        {
            activeLevel = uncompletedLevels.First();
            camps = GameObject.FindObjectsOfType<Camp>().ToList();
            foreach (Camp camp in camps)
            {
                camp.OnCampDie += HandleCampDestroyed;
            }
            playerGUI.UpdateGoals();
        }

    }
    public void SetUpFromLevel(LevelInfo level)
    {
        if (camps.Count > 0)
        {
            foreach (Camp camp in camps)
            {
                camp.OnCampDie -= HandleCampDestroyed;
            }
        }
        camps = GameObject.FindObjectsOfType<Camp>().ToList();
        foreach (Camp camp in camps)
        {
            camp.OnCampDie += HandleCampDestroyed;
        }
        playerGUI.UpdateGoals();
        level.camps = camps.Count;
    }
    public void SpawnBoss(Transform spawnPosition)
    {
        GameObject bossGO = Instantiate(activeLevel.boss, spawnPosition.position, Quaternion.identity);
        Enemy instantiatedBoss = bossGO.GetComponent<Enemy>();
        instantiatedBoss.Setup();
        instantiatedBoss.OnEnemyDie += HandleBossDying;
        boss = instantiatedBoss;
    }
    public void CompleteLevel(LevelInfo level = null)
    {
        if (level == null)
            level = activeLevel;
            
        uncompletedLevels.Remove(level);
        completedLevels.Add(level);
        saveSlotInUse.savedData = SaveSystem.SavePlayer(saveSlotInUse.slot);
    }
    public void SetNextLevel()
    {
        activeLevel = uncompletedLevels.First();
    }
    public Enemy GetBoss() => boss;
    public void HandleCampDestroyed(Camp campment)
    {
        if (GetCampsLeft().Count == 1)
        {
            var lastCampAlive = GetCampsLeft()[0];
            SpawnBoss(lastCampAlive.transform);
        }
        else if (GetCampsLeft().Count == 0 && !boss.Alive())
        {
            CompleteLevel(activeLevel);
        }
        //Trigger message or effect to show that you got 1 camp
        //Also update ui
        playerGUI.UpdateGoals();
    }
    public void HandleBossDying(Enemy boss)
    {
        if (GetCampsLeft().Count == 0)
        {
            CompleteLevel(activeLevel);
            if (uncompletedLevels.Count == 0)
            {
                playerGUI.endMenu.Show();
            }
        }
    }
    public List<Camp> GetCampsLeft() => camps.FindAll(camp => camp.IsAlive());
    public List<Camp> GetCamps() => camps;
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Setup();
    }
    public List<LevelInfo> GetCompletedLevels() => completedLevels;
}
