﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class EnemyAI : MonoBehaviour
{
    private StateMachine _stateMachine;
    private Enemy enemyController;

    public void Awake()
    {
        enemyController = gameObject.GetComponent<Enemy>();
        _stateMachine = new StateMachine();

        IState attackState = new AttackState(enemyController);
        IState passiveState = new PassiveState(enemyController);
        IState hideState = new HideState();
        IState eatState = new EatState(enemyController);
        IState searchForFoodState = new SearchForFoodState(enemyController, 4f);

        At(passiveState, attackState, EnterCombat());
        At(passiveState, searchForFoodState, Starving());
        At(searchForFoodState, passiveState, NotStarvingAnymore());
        At(attackState, passiveState, OutOfCombat());
        At(attackState, hideState, InDangerOfDying());
        At(hideState, passiveState, OutOfCombat());
        At(hideState, attackState, EnterCombat());

        Func<bool> EnterCombat() => () => { return enemyController.isUnderAttack || enemyController.isAggressive; };
        Func<bool> Starving() => () => { return enemyController.IsHungry(); };
        Func<bool> NotStarvingAnymore() => () => { return enemyController.EatenEnough(); };
        Func<bool> OutOfCombat() => () => { return !enemyController.isUnderAttack; };
        Func<bool> InDangerOfDying() => () => { return enemyController.IsDying(); };

        _stateMachine.SetState(passiveState);
        void At(IState from, IState to, Func<bool> condition) => _stateMachine.AddTransition(from, to, condition);
    }

    private void Update() => _stateMachine.Tick();
    private void FixedUpdate() => _stateMachine.FixedTick();

    public string GetCurrentStateName()
    {
        if (_stateMachine.GetCurrentState() != null)
        {
            return _stateMachine?.GetCurrentState().GetType().Name;
        } else
        {
            return "None";
        }
    }
}