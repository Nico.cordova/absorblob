using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class SearchForFoodState : IState
{
    private Enemy _enemy;
    private float _searchCooldown;
    private float _defaultCooldown;
    public SearchForFoodState(Enemy enemy, float searchCooldown)
    {
        _defaultCooldown = searchCooldown;
        _searchCooldown = searchCooldown;
        _enemy = enemy;
    }
    public void Tick() 
    {
    }
    public void FixedTick() {
        if (_searchCooldown > 0f)
        {
            _searchCooldown -= Time.deltaTime;
            _searchCooldown = Mathf.Clamp(_searchCooldown, 0f, Mathf.Infinity);
        }
        if (_enemy.pathfinder.velocity.magnitude == 0 && _searchCooldown == 0f)
        {
            _searchCooldown = _defaultCooldown;
            _enemy.SearchForFood();
        }
    }
    public void OnEnter()
    {
    }
    public void OnExit()
    {
    }
}
