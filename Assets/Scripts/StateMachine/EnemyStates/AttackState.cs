using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class AttackState : IState
{
    private Enemy _enemyController;
    private Animator _animator;
    private float moveCoolDown = 5f;
    private float attackCooldown = 2f;
    public AttackState(Enemy enemyController)
    {
        _enemyController = enemyController;
        _animator = enemyController.gameObject.GetComponentInChildren<Animator>();
    }

    public void Tick()
    {
        moveCoolDown -= Time.deltaTime;
        attackCooldown -= Time.deltaTime;
        if (moveCoolDown <= 0 && attackCooldown > 0)
        {
            _enemyController.MoveRandomly(6f);
            moveCoolDown = 4f;
        }
        else if (attackCooldown <= 0)
        {
            _enemyController.LookAtTarget();
            _animator.SetTrigger("Attack");
            attackCooldown = 2f;
        }
    }
    public void FixedTick() {
    }
    public void OnEnter()
    {
    }
    public void OnExit()
    {
    }
}
