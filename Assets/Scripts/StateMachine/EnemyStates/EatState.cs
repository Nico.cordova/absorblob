using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class EatState : IState
{
    private Enemy _enemy;
    public EatState(Enemy enemy)
    {
        _enemy = enemy;
    }

    public void Tick()
    {
    }
    public void FixedTick() { }
    public void OnEnter()
    {
    }
    public void OnExit()
    {
    }
}
