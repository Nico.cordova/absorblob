using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class PassiveState : IState
{
    private Enemy _enemyController;
    private float moveCoolDown = 4f;
    public PassiveState(Enemy enemyController)
    {
        _enemyController = enemyController;
    }

    public void Tick()
    {
    }
    public void FixedTick()
    {
        moveCoolDown -= Time.deltaTime;
        if (moveCoolDown <= 0)
        {
            _enemyController.MoveRandomly(6f);
            moveCoolDown = 4f;
        }
    }
    public void OnEnter()
    {
        //Debug.Log($"[{_enemyController.gameObject.name}] ENTERED PASSIVE STATE");
    }
    public void OnExit()
    {
        //Debug.Log($"[{_enemyController.gameObject.name}] EXITED PASSIVE STATE");
    }
}
