using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class SaveSlot
{
    public int slot;
    public PlayerSaveData savedData;
    public SaveSlot(int slotIndex, PlayerSaveData saveData)
    {
        slot = slotIndex;
        savedData = saveData;
    }
}
public static class SaveSystem 
{
    public static SaveSlot activeSave;
    public static PlayerSaveData SavePlayer(int saveSlot)
    {
        Debug.Log($"SAVING ACTUAL GAME ON SLOT {saveSlot}");
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + $"/slot-{saveSlot}.save";
        FileStream fs = new FileStream(path, FileMode.Create);
        
        PlayerSaveData data = new PlayerSaveData();
        formatter.Serialize(fs, data);
        fs.Close();
        return data;
    }
    public static PlayerSaveData LoadPlayer (int saveSlot)
    {
        Debug.Log($"LOADING PLAYER ON SLOT {saveSlot}");
        string path = Application.persistentDataPath + $"/slot-{saveSlot}.save";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter ();
            FileStream fileStream = new FileStream ( path, FileMode.Open);
            PlayerSaveData savedData = formatter.Deserialize(fileStream) as PlayerSaveData;
            fileStream.Close();
            return savedData;

        } else
        {
            return null;
        }
    }
}
