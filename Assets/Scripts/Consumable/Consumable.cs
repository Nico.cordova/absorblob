using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static DigestiveSystem;

public class Consumable : MonoBehaviour
{

    public event Action OnEaten;
    [Header("Value")]
    public FoodType foodType;
    public int caloriesWorth = 500;
    public int heartsWorth = 1;
    public List<PowerUpEffect> powerUps = new List<PowerUpEffect>();

    [Header("Configurations")]
    public DigestionTimer timerInfo;
    public bool canRespawn = false;
    public float defaultRespawnTime = 5f;
    public float digestTime = 5f;
    public bool organic = false;
    public bool canBeEaten = true;
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private SpriteRenderer sprite;
    private float respawnTime;
    private Collider2D polygonCollider;
    private EatIndicator eatIndicator;
    
    [Header("DEBUGGING")]
    public float digestTimeLeft;

    private void Start()
    {
        if (timerInfo)
            timerInfo = Instantiate(timerInfo);
        eatIndicator = EatIndicator.Instance;
        if (sprite == null)
        {
            sprite = gameObject.GetComponent<SpriteRenderer>();
        }
        polygonCollider = gameObject.GetComponent<Collider2D>();
        respawnTime = defaultRespawnTime;
        if (_animator == null)
            _animator = gameObject.GetComponent<Animator>();
    }

    public void Highlight()
    {
        eatIndicator.MoveTo((Vector2) transform.position + new Vector2(0f, 0.4f));
        eatIndicator.Show();
        sprite.material = GameManager.Instance.highlightMaterial;
    }
    public void StopHighlight()
    {

        sprite.material = GameManager.Instance.defaultLitMaterial;
        eatIndicator.Hide();
    }
    public bool IsHighlighted()
    {
        return sprite.material == GameManager.Instance.highlightMaterial;
    }
    public void ChangeMaterial(Material newMaterial)
    {
        sprite.material = newMaterial;
    }
    public void GetEaten()
    {
        Disable();
        StopHighlight();
        digestTimeLeft = digestTime;
        timerInfo.percentageLeft = 1;
        OnEaten?.Invoke();
    }
    private void Enable()
    {
        canBeEaten = true;
        polygonCollider.enabled = true;
        sprite.enabled = true;
        _animator.SetTrigger("Spawn");
    }
    private void Disable()
    {
        canBeEaten = false;
        polygonCollider.enabled = false;
        sprite.enabled = false;
    }
    public void RespawnAtPosition(Vector3 position) 
    {
        transform.position = position;
        StartCoroutine(Respawn());
    }

    public void InitRespawn()
    {
        StartCoroutine(Respawn());
    }
    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(respawnTime);
        Enable();
    }
}
