using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poop : MonoBehaviour
{
    private SpriteRenderer sprite;
    void Start()
    {
        sprite = gameObject.GetComponent<SpriteRenderer>();
    }
    void Awake()
    {
        sprite = gameObject.GetComponent<SpriteRenderer>();
    }
    public void SetLifetime(float time)
    {
        Enable();
        StartCoroutine(Dissolve(time));
    }

    private IEnumerator Dissolve(float time)
    {
        yield return new WaitForSeconds(time);
        Disable();
    }
    private void Enable()
    {
        sprite.enabled = true;
    }
    private void Disable()
    {
        sprite.enabled = false;
    }
}
