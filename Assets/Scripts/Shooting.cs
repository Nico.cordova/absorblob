using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform firePoint;
    public float bulletForce = 20f;
    private bool isMainPlayer = false;
    private void Start()
    {
        isMainPlayer = true ? gameObject.GetComponent<PlayerController>() : false;
    }
    public void Shoot(GameObject target)
    {
        Vector3 direction = target.transform.position - firePoint.position;
        float angle = Vector2.SignedAngle(direction, Vector3.up);
        firePoint.transform.rotation = Quaternion.Euler(0, 0, -angle);
        GameObject bullet = ObjectPooler.Instance.SpawnFromPool("StoneProjectile", firePoint.position, firePoint.rotation);
        bullet.SetActive(true);
        Projectile bulletProjectile = bullet.GetComponent<Projectile>();
        if (isMainPlayer)
            bulletProjectile.InitializeAsPlayer();
        else
            bulletProjectile.InitializeAsEnemy();
        bulletProjectile.Shoot(bulletForce, firePoint.up);
    }
    public void Shoot(Vector3 direction)
    {
        float angle = Vector2.SignedAngle(direction, Vector3.up);
        firePoint.transform.rotation = Quaternion.Euler(0, 0, -angle);
        GameObject bullet = ObjectPooler.Instance.SpawnFromPool("StoneProjectile", firePoint.position, firePoint.rotation);
        bullet.SetActive(true);
        Projectile bulletProjectile = bullet.GetComponent<Projectile>();
        if (isMainPlayer)
            bulletProjectile.InitializeAsPlayer();
        else
            bulletProjectile.InitializeAsEnemy();
        bulletProjectile.Shoot(bulletForce, firePoint.up);
    }
}
