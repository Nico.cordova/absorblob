using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;

public class DigestiveSystem : MonoBehaviour
{
    [Header("Configuration")]
    [Range(0,1)]
    public float hungryThreshold;
    [Range(0, 1)]
    public float fullThreshold;
    public float defaultMaxCalories;
    public float defaultInitialCalories;
    public bool canStarveToDeath;
    public float baseMetabolism;
    public float metabolicTime;
    public float defaultStarveDamage;
    public int defaultStomachSize = 3;
    public event Action OnCaloriesChange;
    public List<FoodType> foodICanEat = new List<FoodType>();
    public List<Consumable> consumablesInRange = new List<Consumable>();

    private Collider2D eatingRange;
    private List<Vector3> consumablesSeen = new List<Vector3>();
    private Queue<Vector3> lastPlacesIAte = new Queue<Vector3>();
    private float calories;
    private float maxCalories;
    private float starveDamage;
    private int stomachSize;
    private Hitable hitableController;
    private List<Consumable> eatenConsumables = new List<Consumable>();
    private Consumable lastHighlightedConsumable = null;
    private bool isMainPlayer = false;
    public enum FoodType
    {
        mushroom,
        flower,
        rock,
        meleeEnemy,
        rangedEnemy
    };
    public bool CanEat(Consumable consumable)
    {
        if (consumable != null)
            return foodICanEat.Contains(consumable.foodType) && consumable.canBeEaten && eatenConsumables.Count() < stomachSize;
        else
            return false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Consumable") || collision.CompareTag("Enemy"))
        {
            Consumable collisionConsumable = collision.GetComponent<Consumable>();
            if (collisionConsumable && CanEat(collisionConsumable) && !consumablesInRange.Contains(collisionConsumable))
            {
                consumablesInRange.Add(collisionConsumable);
                if (!consumablesSeen.Contains(collisionConsumable.transform.position))
                    consumablesSeen.Add(collisionConsumable.transform.position);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.CompareTag("Consumable") || collision.CompareTag("Enemy"))
        {
            Consumable collisionConsumable = collision.GetComponent<Consumable>();
            if (CanEat(collisionConsumable))
            {
                if (consumablesInRange.Contains(collisionConsumable))
                {
                    consumablesInRange.Remove(collisionConsumable);
                    if (collisionConsumable.IsHighlighted())
                    {
                        collisionConsumable.StopHighlight();
                    }
                }
            }
        }
    }
    private void Start()
    {
        Setup();
    }
    public void Setup()
    {
        consumablesInRange.Clear();
        StopAllCoroutines();
        if (eatenConsumables.Count > 0)
            eatenConsumables.RemoveAll(item => true);
        calories = defaultInitialCalories;
        StartCoroutine(LivingExpenses());

        if (gameObject.GetComponent<PlayerController>() != null)
        {
            StartCoroutine(HighlightConsumable());
            isMainPlayer = true;
        }
        hitableController = gameObject.GetComponent<Hitable>();
        starveDamage = defaultStarveDamage;
        maxCalories = defaultMaxCalories;
        stomachSize = defaultStomachSize;
    }
    public Consumable GetNearestConsumable()
    {
        if (consumablesInRange.Count != 0)
        {
            Consumable nearest = consumablesInRange.Aggregate((curNearest, consumable) => (curNearest == null || (consumable.transform.position - transform.position).magnitude < (curNearest.transform.position - transform.position).magnitude ? consumable : curNearest)); ;
            return nearest;
        }
        else
        {
            return null;
        }

    }
    public bool ConsumableInRange()
    {
        return consumablesInRange.Count > 0;
    }
    public Vector3 GetLatestPlaceIAte()
    {
        return lastPlacesIAte.Count > 0 ? lastPlacesIAte.Dequeue() : Vector3.negativeInfinity;
    }
    public Vector3 GetLatestConsumableSeen()
    {
        return consumablesSeen.Count > 0 ? consumablesSeen.First() : Vector3.negativeInfinity;
    }
    public void EatConsumable(Consumable item)
    {
        AddCalories(item.caloriesWorth);
        item.GetEaten();
        StartCoroutine(Digest(item));
        eatenConsumables.Add(item);
        lastPlacesIAte.Enqueue(item.transform.position);
        foreach(PowerUpEffect powerUp in item.powerUps)
        {
            Debug.Log($"APPLYING {powerUp.name}");
            powerUp.Apply(gameObject);
        }
        if (consumablesInRange.Contains(item))
        {
            consumablesInRange.Remove(item);
            item.StopHighlight();
        }
        if (consumablesSeen.Contains(item.transform.position))
            consumablesSeen.Remove(item.transform.position);
    }
    public float CaloriesLeft() => calories; 
    public float AddCalories(float caloriesToAdd) {
        if (calories == 0)
        {
            StartCoroutine(LivingExpenses());
        }
        calories += caloriesToAdd;
        calories = Mathf.Clamp(calories, 0, maxCalories);

        OnCaloriesChange?.Invoke();
        return calories;
    }
    public bool IsHungry()
    {
        return calories / maxCalories <= hungryThreshold ;
    }
    public bool EatenEnough()
    {
        return calories / maxCalories >= fullThreshold;
    }
    public float ConsumeCalories(float caloriesToConsume)
    {
        calories -= caloriesToConsume;
        calories = Mathf.Clamp(calories, 0f, maxCalories);
        if (calories == 0)
        {
            StartCoroutine(Starve());
        }
        OnCaloriesChange?.Invoke();
        return calories;
    }

    private void Poop(Consumable item)
    {
        GameObject poopGO = ObjectPooler.Instance.SpawnFromPool("Poop", transform.position, Quaternion.identity);
        Poop poopController = poopGO.GetComponent<Poop>();
        poopController.SetLifetime(item.digestTime);
        item.RespawnAtPosition(transform.position);
        if (eatenConsumables.Contains(item))
        {
            eatenConsumables.Remove(item);
        }
    }
    private IEnumerator Digest(Consumable item)
    {
        if (isMainPlayer)
        {
            DigestionTimerDisplay timer = PlayerGUI.Instance.GetDigestionTimerToUse();
            timer.InitFromConsumable(item);
        }
        while (item.digestTimeLeft > 0)
        {
            yield return new WaitForSeconds(0.1f);
            item.digestTimeLeft -= 0.1f;
            item.digestTimeLeft = Mathf.Clamp(item.digestTimeLeft, 0, Mathf.Infinity);
            item.timerInfo.percentageLeft = item.digestTimeLeft / item.digestTime;
        }
        if (item.canRespawn)
        {
            if (item.organic)
            {
                Poop(item);
            } else
            {
                item.InitRespawn();
                if (eatenConsumables.Contains(item))
                {
                    eatenConsumables.Remove(item);
                }
            }
        }
    }
    private IEnumerator LivingExpenses()
    {
        while( calories > 0)
        {
            yield return new WaitForSeconds(metabolicTime);
            ConsumeCalories(baseMetabolism);
        }
    }

    private IEnumerator Starve()
    {
        while( calories == 0)
        {
            yield return new WaitForSeconds(metabolicTime);
            //Debug.Log($"[{gameObject.name}] STARVING. GETTING HIT WITH {starveDamage}");
            hitableController.GetHit(starveDamage, transform.position);
        }
    }
    private IEnumerator HighlightConsumable()
    {
        PlayerController player = gameObject.GetComponent<PlayerController>();
        while (true)
        {
            yield return new WaitForSeconds(0.05f);

            Consumable nearest = GetNearestConsumable();
            if (nearest != null)
            {
                if (!player.stats.eating && CanEat(nearest))
                {
                    if (nearest != null) //there's a consumable nearby
                    {
                        if (lastHighlightedConsumable != null) //There was another consumable highlighted before
                        { 
                            lastHighlightedConsumable.StopHighlight(); //Stopping highlight of last item highlighted before highlighting new one
                        }
                        nearest.Highlight();
                        lastHighlightedConsumable = nearest;
                    } else //there's no consumable nearby
                    {
                        if (lastHighlightedConsumable != null)
                        {
                            lastHighlightedConsumable.StopHighlight();
                            lastHighlightedConsumable = null;
                        }
                    }
                } else
                {
                    if (lastHighlightedConsumable != null)
                    {
                        lastHighlightedConsumable.StopHighlight();
                        lastHighlightedConsumable = null;
                    }
                }
            }
        }
    }
}
